#-------------------------------------------------
#
# Project created by QtCreator 2015-12-06T15:44:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Dijkstra
TEMPLATE = app


SOURCES += main.cpp\
    quadtree.cpp \
    ../utilities/FileParser/file_parser.cpp \
    dijkstraworker.cpp \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.cpp \
    dijkstra.cpp \
    node.cpp

HEADERS  += \
    quadtree.h \
    ../utilities/FileParser/file_parser.h \
   dijkstraworker.h \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.h \
    dijkstra.h \
    node.h

CONFIG += c++11

FORMS    += mainwindow.ui

DISTFILES += \
    ../Debug/settings.conf
