#ifndef NODE_H
#define NODE_H

#include <QLineF>
#include <QSharedPointer>
#include <QVector>

struct node
{
	public:
    node(const qreal& x = -1,
					const qreal& y = -1);

    QSharedPointer<node> parent() const;
    void setParent(QSharedPointer<node> parent);

		double cost() const;
		void setCost(const double& cost);

    void add_child(QSharedPointer<node> child);
    void remove_child(QSharedPointer<node> child);

    QVector<QSharedPointer<node>> _children;

		double x() const;
		void setX(const double& x);

		double y() const;
		void setY(const double& y);

		QLineF edge() const;
		void setEdge(const QLineF& edge);

    QPoint toPoint() const;
    QPointF toPointF() const;
    void resetNode();

	private:
		double _x, _y;
		QLineF _edge;
    QSharedPointer<node> _parent;
    double _cost;
};

#endif // NODE_H
