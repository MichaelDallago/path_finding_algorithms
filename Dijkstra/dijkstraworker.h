#ifndef DIJKSTRAWORKER_H
#define DIJKSTRAWORKER_H

#include "quadtree.h"

#include <QElapsedTimer>
#include <QObject>
#include <QPolygon>

typedef QList<QPoint> pathType;

class DijkstraWorker : public QObject
{
		Q_OBJECT
	public:
		DijkstraWorker(const uint& mapWidth, const uint& mapHeight,
									 const uint& startingNodeX, const uint& startingNodeY,
									 const uint& targetNodeX, const uint& targetNodeY,
									 const uint& updateInterval,
									 const bool& drawQuadTrees,
									 const QVector<QPolygon>& obstacles,
									 const QVector<QPolygon>& preferredZones,
									 const QVector<QPolygon>& offLimitsZones);
		~DijkstraWorker();

    void requestStop();

	private:
		int mapWidth, mapHeight;
		int startingNodeX, startingNodeY;
		int targetNodeX, targetNodeY;
    int updateInterval;
    bool drawQuadTrees;
    QVector<QPolygon> obstacles, preferredZones, offLimitsZones;

    QImage image;
    bool stop;
		QVector<QLineF> obstaclesEdges;
    QElapsedTimer elapsedTime, localTimer;
    QSharedPointer<QuadTree> mainQuadTree;

		pathType path;
    qreal localTargetsCosts;
    QSharedPointer<node> startingNode, targetNode;
    QVector<QSharedPointer<node>> graph, preferredZonesCenters;

		void setObstaclesEdges();
    void setOffLimitsEdges();
    void setPreferredZones();

    bool obstacleFree(QSharedPointer<node> src, QSharedPointer<node> dst);
    void drawObstacle(const QPolygon& obstacle);
    void drawOffLimitsZone(const QPolygon& offLimitsZone);
    void drawPreferredZone(const QPolygon& preferredZone);
    void drawNode(QSharedPointer<node> node, const int& radius = 1.0, const QColor& color = Qt::black);
    void drawRect(const QRectF& rect);

    void setupPreferredZonesCenters();
    void setupGraph();
    void searchFreeQuads(QSharedPointer<QuadTree> quadTree);
    QSharedPointer<node> cheapest(QVector<QSharedPointer<node>>& Q);
    void resetNodes(QVector<QSharedPointer<node>>& Q);

    void initImage();
    void maybeEmitUpdatedImage();

    void generatePath(QSharedPointer<node> src, QSharedPointer<node> dst);

	signals:
		void updatedImage(QImage, qreal, qint64);
    void updatedPath(pathType);
		void finished();

	public slots:
		void start();
};

#endif // DIJKSTRAWORKER_H
