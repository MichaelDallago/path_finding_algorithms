#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "dijkstraworker.h"
#include "../utilities/ZoomableGraphicsView/zoomablegraphicsview.h"

#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QThread>

namespace Ui {
  class MainWindow;
}

class DijkstraWindow : public QMainWindow
{
    Q_OBJECT

  public:
    DijkstraWindow(const uint& mapWidth, const uint& mapHeight,
                   const uint& startingNodeX, const uint& startingNodeY,
                   const uint& targetNodeX, const uint& targetNodeY,
                   const uint& updateInterval,
                   const bool& drawQuadtreesBoundaries,
                   const QVector<QPolygon>& obstacles,
                   const QVector<QPolygon>& preferredZones,
                   const QVector<QPolygon>& offLimitsZones,
                   QWidget *parent = 0);
    ~DijkstraWindow();

  private:
    Ui::MainWindow* ui;
    QGraphicsScene* graphicsScene;
    QScopedPointer<DijkstraWorker> workerThread;
    QScopedPointer<QThread> thread;

    uint mapWidth, mapHeight;
    uint startingNodeX, startingNodeY;
    uint targetNodeX, targetNodeY;
    uint updateInterval;
    bool drawQuadtreesBoundaries;
    QVector<QPolygon> obstacles, preferredZones, offLimitsZones;
    QVector<QLineF> obstaclesEdges;
    pathType pathNodes;

    void begin();
    void savePathToFile(const QString& fileName);

  private slots:
    void updateImage(QImage image, qreal cost, qint64 elapsedTime);
    void catchPath(pathType pathVertices);
    void on_actionSave_path_as_triggered();
    void on_stopButton_clicked();
};

#endif // DIJKSTRA_H
