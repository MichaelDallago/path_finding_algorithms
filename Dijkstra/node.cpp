#include "node.h"
#include <assert.h>
#include <QtMath>

node::node(const qreal& x, const qreal& y) :
  _x(x), _y(y),
  _parent(nullptr), _cost(INT64_MAX - 1)
{}

QSharedPointer<node> node::parent() const
{
	return _parent;
}

void node::setParent(QSharedPointer<node> parent)
{
	_parent = parent;
}

qreal node::cost() const
{
	return _cost;
}

void node::setCost(const qreal& cost)
{
	_cost = cost;
}

void node::add_child(QSharedPointer<node> child)
{
	_children.push_back(child);
}

void node::remove_child(QSharedPointer<node> child)
{
	bool found = false;
	for (int i = 0; i < _children.size() && !found; ++i)
		if (_children[i] == child)
		{
			_children.removeAt(i);
			found = true;
		}
}

qreal node::x() const
{
	return _x;
}

void node::setX(const qreal& x)
{
	_x = x;
}

qreal node::y() const
{
	return _y;
}

void node::setY(const qreal& y)
{
	_y = y;
}

QLineF node::edge() const
{
	return _edge;
}

void node::setEdge(const QLineF& edge)
{
  _edge = edge;
}

QPoint node::toPoint() const
{
  return QPoint(_x, _y);
}

QPointF node::toPointF() const
{
  return QPointF(_x, _y);
}

void node::resetNode()
{
  _cost = INT64_MAX - 1;
  _parent.reset(nullptr);
  _edge = QLineF();
}
