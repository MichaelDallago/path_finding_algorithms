#include "dijkstraworker.h"

#include <assert.h>
#include <iostream>
#include <QPainter>

DijkstraWorker::DijkstraWorker(const uint& mapWidth, const uint& mapHeight,
                               const uint& startingNodeX, const uint& startingNodeY,
                               const uint& targetNodeX, const uint& targetNodeY,
                               const uint& updateInterval,
                               const bool& drawQuadTrees,
                               const QVector<QPolygon>& obstacles,
                               const QVector<QPolygon>& preferredZones,
                               const QVector<QPolygon>& offLimitsZones) :
  mapWidth(mapWidth), mapHeight(mapHeight),
  startingNodeX(startingNodeX), startingNodeY(startingNodeY),
  targetNodeX(targetNodeX), targetNodeY(targetNodeY),
  updateInterval(updateInterval),
  drawQuadTrees(drawQuadTrees),
  obstacles(obstacles),
  preferredZones(preferredZones),
  offLimitsZones(offLimitsZones),
  image(mapWidth + 4, mapHeight + 4, QImage::Format_ARGB32),
  stop(false),
  localTargetsCosts(0),
  startingNode(nullptr), targetNode(nullptr)
{
  qRegisterMetaType<pathType>("pathType");

  image.fill(Qt::white);
}

DijkstraWorker::~DijkstraWorker()
{}

void DijkstraWorker::requestStop()
{
  stop = true;
}

void DijkstraWorker::setObstaclesEdges()
{
  for (int i = 0; i < obstacles.size(); ++i)
  {
    for (int j = 0; j < obstacles[i].size(); ++j)
    {
      int size = obstacles[i].size();
      if (j < size - 1)
        obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][j + 1]));
      else
        obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][0]));
    }

    drawObstacle(obstacles[i]);
  }
}

void DijkstraWorker::setOffLimitsEdges()
{
  for (int i = 0; i < offLimitsZones.size(); ++i)
  {
    for (int j = 0; j < offLimitsZones[i].size(); ++j)
    {
      int size = offLimitsZones[i].size();
      if (j < size - 1)
        obstaclesEdges.push_back(QLineF(offLimitsZones[i][j], offLimitsZones[i][j + 1]));
      else
        obstaclesEdges.push_back(QLineF(offLimitsZones[i][j], offLimitsZones[i][0]));
    }

    drawOffLimitsZone(offLimitsZones[i]);
  }
}

void DijkstraWorker::setPreferredZones()
{
  foreach(QPolygon preferredZone, preferredZones)
    drawPreferredZone(preferredZone);
}

bool DijkstraWorker::obstacleFree(QSharedPointer<node> src, QSharedPointer<node> dst)
{
  QLineF edge(src->toPointF(), dst->toPointF());
  QPointF p;
  for(int i = 0; i < obstaclesEdges.size(); ++i)
    if (obstaclesEdges[i].intersect(edge, &p) == QLineF::BoundedIntersection)
      return false;

  return true;
}

void DijkstraWorker::drawObstacle(const QPolygon& obstacle)
{
  QPainter painter;
  painter.begin(&image);
  painter.setBrush(Qt::gray);
  painter.drawPolygon(obstacle);
  painter.end();
}

void DijkstraWorker::drawOffLimitsZone(const QPolygon& offLimitsZone)
{
  QPainter painter;
  painter.begin(&image);
  painter.setBrush(Qt::red);
  painter.drawPolygon(offLimitsZone);
  painter.end();
}

void DijkstraWorker::drawPreferredZone(const QPolygon& preferredZone)
{
  QPainter painter;
  painter.begin(&image);
  painter.setBrush(Qt::green);
  painter.drawPolygon(preferredZone);
  painter.end();
}

void DijkstraWorker::drawNode(QSharedPointer<node> node, const int& radius, const QColor& color)
{
  QPainter painter;
  painter.begin(&image);
  painter.setPen(QPen(color));
  painter.setBrush(QBrush(color));
  painter.drawEllipse(node->toPointF(), radius, radius);
  painter.end();
}

void DijkstraWorker::drawRect(const QRectF& rect)
{
  QPainter painter;
  painter.begin(&image);
  painter.setPen(QPen(Qt::blue));
  painter.drawRect(rect);
  painter.end();
}

void DijkstraWorker::setupPreferredZonesCenters()
{
  foreach(QPolygon preferredZone, preferredZones)
  {
    QPoint center = preferredZone.boundingRect().center();
    preferredZonesCenters.push_back(QSharedPointer<node>(new node(center.x(),
                                                                    center.y())));
    drawNode(preferredZonesCenters.last());
  }
  preferredZonesCenters.push_back(targetNode);
}

void DijkstraWorker::setupGraph()
{
  searchFreeQuads(mainQuadTree);
}

void DijkstraWorker::searchFreeQuads(QSharedPointer<QuadTree> quadTree)
{
  if (!quadTree.isNull())
  {
    if (quadTree->getNode() == nullptr)
    {
      graph.push_back(QSharedPointer<node>(new node((quadTree->boundary().left() + quadTree->boundary().right()) / 2,
                                                      (quadTree->boundary().top() + quadTree->boundary().bottom()) / 2)));
      drawNode(graph.last());

      if (drawQuadTrees)
        drawRect(quadTree->boundary());
    }
    else
    {
      searchFreeQuads(quadTree->north_west());
      searchFreeQuads(quadTree->north_east());
      searchFreeQuads(quadTree->south_west());
      searchFreeQuads(quadTree->south_east());
    }
  }
}

QSharedPointer<node> DijkstraWorker::cheapest(QVector<QSharedPointer<node>>& Q)
{
  qreal cost = INT64_MAX;
  QSharedPointer<node> u;
  int index = -1;
  for (int i = 0; i < Q.size(); ++i)
  {
    if (Q[i]->cost() < cost)
    {
      cost = Q[i]->cost();
      u = Q[i];
      index = i;
    }
  }

  Q.removeAt(index);
  return u;
}

void DijkstraWorker::resetNodes(QVector<QSharedPointer<node> >& Q)
{
  foreach(QSharedPointer<node> node, Q)
    node->resetNode();
}

void DijkstraWorker::maybeEmitUpdatedImage()
{
  if (localTimer.elapsed() > updateInterval)
  {
    emit updatedImage(image, targetNode->cost(), elapsedTime.elapsed());
    localTimer.restart();
  }
}

void DijkstraWorker::generatePath(QSharedPointer<node> src, QSharedPointer<node> dst)
{
  QSharedPointer<node> iterator = src;
  QList<QPoint> localPath;
  QPainter painter;
  painter.begin(&image);
  painter.setPen(QPen(Qt::blue, 2.0));
  while (iterator != dst)
  {
    painter.drawLine(iterator->edge());
    localPath.push_front(iterator->toPoint());
    iterator = iterator->parent();
  }
  painter.end();

  if (dst == startingNode)
    localPath.push_front(iterator->toPoint());

  path += localPath;
}

void DijkstraWorker::start()
{
  elapsedTime.start();
  localTimer.start();

  setObstaclesEdges();
  setOffLimitsEdges();
  setPreferredZones();
  startingNode.reset(new node(startingNodeX, startingNodeY));
  targetNode.reset(new node(targetNodeX, targetNodeY));
  drawNode(startingNode, 5, Qt::green);
  drawNode(targetNode, 5, Qt::red);

  mainQuadTree.reset(new QuadTree(QRectF(0, 0, mapWidth, mapHeight)));
  foreach(QPolygon obstacle, obstacles)
  {
    maybeEmitUpdatedImage();
    QRectF bRect = obstacle.boundingRect();
    for (int i = bRect.left(); i < bRect.left() + bRect.width(); ++i)
      for (int j = 0; j < bRect.top() + bRect.height(); ++j)
        if (obstacle.containsPoint(QPoint(i, j), Qt::OddEvenFill))
          mainQuadTree->insert(QSharedPointer<node>(new node(i, j)));
  }
  foreach(QPolygon offLimitsZone, offLimitsZones)
  {
    maybeEmitUpdatedImage();
    QRectF bRect = offLimitsZone.boundingRect();
    for (int i = bRect.left(); i < bRect.left() + bRect.width(); ++i)
      for (int j = 0; j < bRect.top() + bRect.height(); ++j)
        if (offLimitsZone.containsPoint(QPoint(i, j), Qt::OddEvenFill))
          mainQuadTree->insert(QSharedPointer<node>(new node(i, j)));
  }
  setupGraph();

  QVector<QSharedPointer<node>> Q;

  setupPreferredZonesCenters();
  QSharedPointer<node> localStartingNode = startingNode, localTargetNode;
  for(int i = 0; i < preferredZonesCenters.size() && !stop; ++i)
  {
    Q = graph;

    Q.push_back(localStartingNode);
    localTargetNode = preferredZonesCenters[i];
    Q.push_back(localTargetNode);

    resetNodes(Q);

    localStartingNode->setCost(0);
    QSharedPointer<node> u;
    while(!Q.isEmpty() && u != localTargetNode && !stop)
    {
      maybeEmitUpdatedImage();
      u = cheapest(Q);

      foreach(QSharedPointer<node> node, Q)
        if (obstacleFree(u, node))
          u->add_child(node);

      foreach(QSharedPointer<node> v, u->_children)
      {
        QLineF edge(v->toPointF(), u->toPointF());
        qreal tmp = u->cost() + edge.length();
        if (tmp < v->cost())
        {
          v->setCost(tmp);
          v->setParent(u);
          v->setEdge(edge);
        }
      }
    }

    if (!stop)
    {
      generatePath(localTargetNode, localStartingNode);
      localTargetsCosts += localTargetNode->cost();

      localStartingNode = localTargetNode;
    }
  }

  emit updatedImage(image, localTargetsCosts, elapsedTime.elapsed());
  emit updatedPath(path);

  emit finished();
}
