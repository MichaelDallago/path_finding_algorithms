#include <dijkstra.h>
#include <../utilities/FileParser/file_parser.h>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QProcess>

void showVersion()
{
  std::cout << "Dijkstra algorithm, version 1.0" << std::endl;
  std::cout << "Changelog:\n"
            << "v1.0:\n"
            << " - Fully working with off limits and preferred zones.\n"
            << "v0.2.1:\n"
            << " - Added simple command line parser.\n\n"
            << "v0.2:\n"
            << " - Working base Dijkstra.\n\n"
            << "v0.1:\n"
            << " - Initial setup with quadtrees.\n "
            << std::endl;
}

void check_and_correct_values(uint& map_width, uint& map_height)
{
  map_width--;
  map_width |= map_width >> 1;
  map_width |= map_width >> 2;
  map_width |= map_width >> 4;
  map_width |= map_width >> 8;
  map_width |= map_width >> 16;
  map_width++;

  map_height--;
  map_height |= map_height >> 1;
  map_height |= map_height >> 2;
  map_height |= map_height >> 4;
  map_height |= map_height >> 8;
  map_height |= map_height >> 16;
  map_height++;

  uint max = std::max(map_width, map_height);
  map_width = max;
  map_height = max;
}

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QCommandLineParser parser;
  parser.setApplicationDescription("Dijkstra-based motion planner");

  QCommandLineOption configurationFileOption(QStringList() << "f" << "file", "The configuration file to parse.", "conf_file");
  configurationFileOption.setDefaultValue("../settings.conf");
  parser.addOption(configurationFileOption);
  const QCommandLineOption helpOption = parser.addHelpOption();
  const QCommandLineOption versionOption = parser.addVersionOption();

  if (!parser.parse(app.arguments()))
  {
    std::cerr << "Bad command line option: " << parser.errorText().toStdString() << std::endl;
    parser.showHelp();
    return 0;
  }

  if (parser.isSet(versionOption))
  {
    showVersion();
    return 0;
  }

  if (parser.isSet(helpOption))
  {
    parser.showHelp();
    return 0;
  }

  const QString fileName = parser.value(configurationFileOption);
  QFile configFile(fileName);
  if (!configFile.open(QIODevice::ReadOnly | QIODevice::Text))
    return 0;
  else
    configFile.close();

  FileParser fileParser(fileName);
  uint mapWidth = fileParser.getMapWidth(), mapHeight = fileParser.getMapHeight();
  check_and_correct_values(mapWidth, mapHeight);

  DijkstraWindow dijkstraWindow(mapWidth, mapHeight,
                                fileParser.getStartingNodeX(), fileParser.getStartingNodeY(),
                                fileParser.getTargetNodeX(), fileParser.getTargetNodeY(),
                                fileParser.getDijkstraUpdateInterval(),
                                fileParser.getDrawQuadTreesBoundaries(),
                                fileParser.getObstacles(),
                                fileParser.getPreferredZones(),
                                fileParser.getOffLimitsZones());
  dijkstraWindow.show();

  return app.exec();
}
