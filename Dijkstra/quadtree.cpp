#include "quadtree.h"
#include <assert.h>
#include <QDebug>

QuadTree::QuadTree(QRectF boundary) :
  _boundary(boundary),
  _node(nullptr),
  _north_west(nullptr),
  _north_east(nullptr),
  _south_west(nullptr),
  _south_east(nullptr)
{}

QuadTree::~QuadTree()
{}

QRectF QuadTree::boundary() const
{
  return _boundary;
}

QSharedPointer<QuadTree> QuadTree::north_west() const
{
  return _north_west;
}

QSharedPointer<QuadTree> QuadTree::north_east() const
{
  return _north_east;
}

QSharedPointer<QuadTree> QuadTree::south_west() const
{
  return _south_west;
}

QSharedPointer<QuadTree> QuadTree::south_east() const
{
  return _south_east;
}

node* QuadTree::getNode() const
{
  return _node.data();
}

void QuadTree::setNode(QSharedPointer<node> n)
{
  _node = n;
}

bool QuadTree::insert(QSharedPointer<node> n)
{
  if (n->x() < _boundary.left() || n->x() >= _boundary.right() ||
      n->y() < _boundary.top() || n->y() >= _boundary.bottom())
    return false;

  // If there is space in this quad tree, add the object here
  if (_node.isNull())
  {
    _node = n;
    if (_boundary.width() >= 2)
      subdivide(_node);
    return true;
  }

  // Otherwise, subdivide and then add the node to whichever node will accept it
  if (_north_west.isNull())
    subdivide(_node);

  if (_north_west->insert(n))
    return true;
  if (_north_east->insert(n))
    return true;
  if (_south_west->insert(n))
    return true;
  if (_south_east->insert(n))
    return true;

  // Otherwise, the node cannot be inserted for some unknown reason (this should never happen)
  qDebug() << "Very big and bad error happened while trying to insert" << n->toPointF() << "into quad" << _boundary;
  return false;
}

void QuadTree::subdivide(QSharedPointer<node> n)
{
  assert (_north_west.isNull() && _north_east.isNull() && _south_west.isNull() && _south_east.isNull());
  QRectF vertices (_boundary.topLeft(), _boundary.size() / 2.0);

  QPointF nw = vertices.topLeft();
  QPointF ne = vertices.topRight();
  QPointF sw = vertices.bottomLeft();
  QPointF se = vertices.bottomRight();

  _north_west.reset(new QuadTree (QRectF(nw, vertices.size())));
  _north_east.reset(new QuadTree (QRectF(ne, vertices.size())));
  _south_west.reset(new QuadTree (QRectF(sw, vertices.size())));
  _south_east.reset(new QuadTree (QRectF(se, vertices.size())));

  if (n->x() < vertices.right())
  {
    if (n->y() < vertices.bottom())
      _north_west->insert(n);
    else
      _south_west->insert(n);
  }
  else
  {
    if (n->y() < vertices.bottom())
      _north_east->insert(n);
    else
      _south_east->insert(n);
  }
}
