#include "dijkstraworker.h"
#include "dijkstra.h"
#include "ui_mainwindow.h"
#include <iostream>

#include <QKeyEvent>
#include <QFileDialog>
#include <QTimer>
#include <QTextStream>

#define DEFAULT_PATH_FILE "../Dijkstrapath"

DijkstraWindow::DijkstraWindow(const uint& mapWidth, const uint& mapHeight,
                               const uint& startingNodeX, const uint& startingNodeY,
                               const uint& targetNodeX, const uint& targetNodeY,
                               const uint& updateInterval, const bool& drawQuadtreesBoundaries,
                               const QVector<QPolygon>& obstacles,
                               const QVector<QPolygon>& preferredZones,
                               const QVector<QPolygon>& offLimitsZones,
                               QWidget* parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  workerThread(nullptr), thread(nullptr),
  mapWidth(mapWidth), mapHeight(mapHeight),
  startingNodeX(startingNodeX), startingNodeY(startingNodeY),
  targetNodeX(targetNodeX), targetNodeY(targetNodeY),
  updateInterval(updateInterval), drawQuadtreesBoundaries(drawQuadtreesBoundaries),
  obstacles(obstacles), preferredZones(preferredZones), offLimitsZones(offLimitsZones)
{
  ui->setupUi(this);
  graphicsScene = new QGraphicsScene(this);
  graphicsScene->setSceneRect(0, 0, mapWidth + 4, mapHeight + 4);

  ui->graphicsView->setScene(graphicsScene);
  ui->graphicsView->enableZoom();

  ui->menuBar->setEnabled(false);

  QTimer::singleShot(0, this, &DijkstraWindow::begin);
}

DijkstraWindow::~DijkstraWindow()
{
  workerThread->requestStop();
  thread->wait();

  delete ui;
}

void DijkstraWindow::begin()
{
  workerThread.reset(new DijkstraWorker(mapWidth, mapHeight,
                                        startingNodeX, startingNodeY,
                                        targetNodeX, targetNodeY,
                                        updateInterval, drawQuadtreesBoundaries,
                                        obstacles, preferredZones, offLimitsZones));
  thread.reset(new QThread);
  workerThread->moveToThread(thread.data());

  connect(thread.data(), &QThread::started, workerThread.data(), &DijkstraWorker::start);
  connect(workerThread.data(), &DijkstraWorker::updatedImage, this, &DijkstraWindow::updateImage);
  connect(workerThread.data(), &DijkstraWorker::updatedPath, this, &DijkstraWindow::catchPath);
  connect(workerThread.data(), &DijkstraWorker::finished,  [&]()
  {
    ui->stopButton->setEnabled(false);
  });
  connect(workerThread.data(), &DijkstraWorker::finished, thread.data(), &QThread::quit, Qt::DirectConnection);
  thread->start();
}

void DijkstraWindow::updateImage(QImage image, qreal cost, qint64 elapsedTime)
{
  graphicsScene->clear();
  graphicsScene->addPixmap(QPixmap::fromImage(image));
  ui->pathCostLCDNumber->display((double) cost);
  ui->elapsedTimeLCDNumber->display((int) elapsedTime);
}

void DijkstraWindow::catchPath(pathType pathVertices)
{
  pathNodes = pathVertices;
  savePathToFile(DEFAULT_PATH_FILE);
  ui->menuBar->setEnabled(true);
}

void DijkstraWindow::on_actionSave_path_as_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  tr("Save file"),
                                                  "../");
  savePathToFile(fileName);
}

void DijkstraWindow::savePathToFile(const QString& fileName)
{
  QFile file(fileName);

  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    std::cerr << "Failed to create file " << file.fileName().toStdString() << ": " << strerror(errno) << ".\n" << std::endl;
    return;
  }

  QTextStream out(&file);
  for (int i = 0 ; i < pathNodes.size(); ++i)
    out << pathNodes[i].x() << " " << pathNodes[i].y() << "\n";

  file.close();

  ui->statusBar->showMessage("File " + fileName + " successfully saved.");
}

void DijkstraWindow::on_stopButton_clicked()
{
  ui->stopButton->setEnabled(false);
  workerThread->requestStop();
}
