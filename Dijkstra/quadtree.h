#ifndef QUADTREE_H
#define QUADTREE_H

#include "node.h"

#include <QGraphicsScene>
#include <QRectF>
#include <QVector>

#define CAPACITY 1

class QuadTree
{
	private:
    QRectF _boundary;
    QSharedPointer<node> _node;
    QSharedPointer<QuadTree> _north_west;
    QSharedPointer<QuadTree> _north_east;
    QSharedPointer<QuadTree> _south_west;
    QSharedPointer<QuadTree> _south_east;

	public:
    QuadTree (QRectF boundary);
    ~QuadTree();
    bool insert(QSharedPointer<node> n);
    void subdivide(QSharedPointer<node> n);
    QRectF boundary() const;
    QSharedPointer<QuadTree> north_west() const;
    QSharedPointer<QuadTree> north_east() const;
    QSharedPointer<QuadTree> south_west() const;
    QSharedPointer<QuadTree> south_east() const;
    node* getNode() const;
    void setNode(QSharedPointer<node> n);
};

#endif // QUADTREE_H
