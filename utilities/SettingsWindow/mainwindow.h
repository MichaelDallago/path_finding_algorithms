#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../FileParser/file_parser.h"
#include "../ZoomableGraphicsView/zoomablegraphicsview.h"

#include <QGraphicsPolygonItem>
#include <QMainWindow>
#include <QProcess>
#include <QStandardItemModel>

namespace Ui {
	class MainWindow;
}

class SettingsWindow : public QMainWindow
{
		Q_OBJECT

	public:
		SettingsWindow(QWidget *parent = 0);
		~SettingsWindow();

		void setCurrentConfigurationFile(const QString& fileName);
		void setMapSize(const uint& mapWidth, const uint& mapHeight);
		void resetMapSize();
		void setStartingNode(const uint& x, const uint& y);
		void resetStartingNode();
		void setTargetNode(const uint& x, const uint& y);
		void resetTargetNode();
		void setBuildTargetSubtree(const bool& buildTargetSubtree);
		void setMaxStepSize(const double& maxStepSize);
		void setMaxNodesNumber(const uint& maxNodesNumber);
		void setRRTUpdateInterval(const uint& updateInterval);
		void setMaxNeighboursRadius(const double& maxNeighboursRadius);
		void setRRTStarUpdateInterval(const uint& updateInterval);
    void setTreeRedrawInterval(const uint& treeRedrawInterval);
		void setDijkstraUpdateInterval(const uint& DijkstraUpdateInterval);
		void setDrawQuadTreesBoundaries(const bool&drawQuadtreesBoundaries);
		void setObstacles(const QVector<QPolygon>& obstacles);
		void setOffLimitsZones(const QVector<QPolygon>& offLimitsZones);
		void setPreferredZones(const QVector<QPolygon>& preferredZones);
		void setUIFields();

		// QWidget interface
	protected:
		void closeEvent(QCloseEvent* closeEvent);

	private:
		Ui::MainWindow *ui;
		QGraphicsScene *graphicsScene;
		QStandardItemModel obstaclesItemModel, preferredZonesItemModel, offLimitsZonesItemModel;
		int oldObstaclesIndex, oldPreferredZonesIndex, oldOffLimitsZonesIndex;
		QString obstaclesTempRow, preferredZonesTempRow, offLimitsZonesTempRow;
		bool startedAddingObstacleVertices, startedAddingPreferredZoneVertices, startedAddingOffLimitsZoneVertices;
		QString currentConfigurationFile;
		QVector<bool> unsavedChanges;

		uint mapWidth, mapHeight;
		uint startingNodeX, startingNodeY;
		uint targetNodeX, targetNodeY;
		QGraphicsEllipseItem* graphicalStartingNode, * graphicalTargetNode;

		bool buildTargetSubtree;
		uint maxNodesNumber;
		double maxStepSize;
		uint RRTUpdateInterval;
		QVector<QProcess*> RRTProcesses;

		double maxNeighboursRadius;
		uint RRTStarUpdateInterval;
    uint treeRedrawInterval;
		QVector<QProcess*> RRTStarProcesses;

		uint DijkstraUpdateInterval;
		bool drawQuadtreesBoundaries;
		QVector<QProcess*> DijkstraProcesses;

		QVector<QPolygon> obstacles;
		QVector<QGraphicsPolygonItem*> graphicalObstacles;
		QVector<QPoint> obstacleVertices;
		QVector<QGraphicsEllipseItem*> graphicalObstacleVertices;

		QVector<QPolygon> offLimitsZones;
		QVector<QGraphicsPolygonItem*> graphicalOffLimitsZones;
		QVector<QPoint> offLimitsZoneVertices;
		QVector<QGraphicsEllipseItem*> graphicalOffLimitsZoneVertices;

		QVector<QPolygon> preferredZones;
		QVector<QGraphicsPolygonItem*> graphicalPreferredZones;
		QVector<QPoint> preferredZoneVertices;
		QVector<QGraphicsEllipseItem*> graphicalPreferredZoneVertices;

		const char* printOrdinalSuffix(const uint& i);
		void loadFile(const QString& fileName);
		void saveFile(const QString& fileName);
		void maybeSave();

	private slots:
		void printMouseCoordinates(const qreal& x, const qreal& y);
		void drawPoint(const qreal& x, const qreal& y);

		void on_mapSizeSetButton_clicked();
		void on_mapSizeResetButton_clicked();
		void on_startingNodeSetButton_clicked();
		void on_startingNodeResetButton_clicked();
		void on_targetNodeSetButton_clicked();
		void on_targetNodeResetButton_clicked();

		void on_buildTargetSubtreeCheckbox_clicked();
		void on_maxNodesNumberSpinBox_editingFinished();
		void on_maxStepSizeDoubleSpinBox_editingFinished();
		void on_RRTUpdateIntervalSpinBox_editingFinished();
		void on_startRRTButton_clicked();

		void on_maxNeighboursRadiusDoubleSpinBox_editingFinished();
		void on_RRTStarUpdateIntervalSpinBox_editingFinished();
    void on_treeRedrawIntervalSpinBox_editingFinished();
		void on_startRRTStarButton_clicked();

		void on_addObstacleButton_clicked();
		void on_drawObstacleButton_clicked();
		void on_clearObstacleButton_clicked();
		void on_clearAllObstaclesButton_clicked();

		void on_addPreferredZoneButton_clicked();
		void on_drawPreferredZoneButton_clicked();
		void on_clearPreferredZoneButton_clicked();
		void on_clearAllPreferredZonesButton_clicked();

		void on_addOffLimitsZoneButton_clicked();
		void on_drawOffLimitsZoneButton_clicked();
		void on_clearOffLimitsZoneButton_clicked();
		void on_clearAllOffLimitsZonesButton_clicked();

		void on_actionLoad_triggered();
		void on_actionSave_triggered();
		void on_actionSave_as_triggered();
		void on_actionExit_triggered();
		void on_actionAbout_triggered();
		void on_DijkstraUpdateIntervalSpinBox_editingFinished();
		void on_drawQuadtreesBoundariesCheckBox_clicked();
		void on_startDijkstraButton_clicked();
};

#endif // MAINWINDOW_H
