#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>
#include <iomanip>
#include <QFileDialog>
#include <QMessageBox>
#include <QScrollBar>
#include <QProcess>

#define NODE_RADIUS 10
#define DEFAULT_CONF_FILE "../../settings.conf"

QString polygon_to_string (const QPolygon& p)
{
  QString s_p;

  for (int i = 0; i < p.size(); ++i)
    s_p.append(QString::number(p[i].x()) + "," + QString::number(p[i].y()) + "; ");

  return s_p;
}

SettingsWindow::SettingsWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  startedAddingObstacleVertices(false),
  startedAddingPreferredZoneVertices(false),
  startedAddingOffLimitsZoneVertices(false),
  graphicalStartingNode(nullptr), graphicalTargetNode(nullptr)
{
  ui->setupUi(this);

  ui->obstaclesListView->setModel(&obstaclesItemModel);
  ui->preferredZonesListView->setModel(&preferredZonesItemModel);
  ui->offLimitsZonesListView->setModel(&offLimitsZonesItemModel);

  graphicsScene = new QGraphicsScene(this);
  graphicsScene->setSceneRect(0, 0, 200, 200);

  ui->graphicsView->setScene(graphicsScene);
  ui->graphicsView->setMinimumSize(200, 200);
  ui->graphicsView->enableZoom();
  connect(ui->graphicsView, &ZoomableGraphicsView::mouse_moved, this, &SettingsWindow::printMouseCoordinates);
  connect(ui->graphicsView, &ZoomableGraphicsView::mouse_clicked, this, &SettingsWindow::drawPoint);

  ui->drawObstacleButton->setEnabled(false);
  ui->drawPreferredZoneButton->setEnabled(false);
  ui->drawOffLimitsZoneButton->setEnabled(false);

  unsavedChanges.resize(16);
  unsavedChanges.fill(false);
}

SettingsWindow::~SettingsWindow()
{
  delete ui;
}

void SettingsWindow::setCurrentConfigurationFile(const QString& fileName)
{
  currentConfigurationFile = fileName;
}

void SettingsWindow::setMapSize(const uint& mapWidth, const uint& mapHeight)
{
  ui->infoMessagesTextEdit->append("Setting map size:");
  this->mapWidth = mapWidth;
  ui->infoMessagesTextEdit->append(" => Map width successfully set to " + QString::number(mapWidth));
  this->mapHeight = mapHeight;
  ui->infoMessagesTextEdit->append(" => Map height successfully set to " + QString::number(mapHeight));

  graphicsScene->setSceneRect(-NODE_RADIUS / 2, -NODE_RADIUS / 2, mapWidth + NODE_RADIUS, mapHeight + NODE_RADIUS);
  ui->graphicsView->setMaximumSize(mapWidth + NODE_RADIUS, mapHeight + NODE_RADIUS);

  ui->mapSizeSetButton->setEnabled(false);
  ui->mapSizeResetButton->setEnabled(true);

  ui->startingNodeXSpinBox->setMaximum(mapWidth);
  ui->startingNodeYSpinBox->setMaximum(mapHeight);
  ui->targetNodeXSpinBox->setMaximum(mapWidth);
  ui->targetNodeYSpinBox->setMaximum(mapHeight);
}

void SettingsWindow::resetMapSize()
{
  ui->mapSizeSetButton->setEnabled(true);
  ui->mapSizeResetButton->setEnabled(false);
}

void SettingsWindow::setStartingNode(const uint& x, const uint& y)
{
  ui->infoMessagesTextEdit->append("Setting starting node:");
  startingNodeX = x;
  ui->infoMessagesTextEdit->append(" => Starting node X coordinate successfully set to " + QString::number(startingNodeX));
  startingNodeY = y;
  ui->infoMessagesTextEdit->append(" => Starting node Y coordinate successfully set to " + QString::number(startingNodeY));

  graphicalStartingNode = graphicsScene->addEllipse(QRectF(QPoint(startingNodeX - 3.5, startingNodeY - 3.5), QSize(NODE_RADIUS, NODE_RADIUS)), QPen(Qt::green), QBrush(Qt::green));

  ui->startingNodeSetButton->setEnabled(false);
  ui->startingNodeResetButton->setEnabled(true);
}

void SettingsWindow::resetStartingNode()
{
  ui->infoMessagesTextEdit->append("Starting node coordinates successfully reset.");

  startingNodeX = 0;
  startingNodeY = 0;

  delete graphicalStartingNode;

  ui->startingNodeSetButton->setEnabled(true);
  ui->startingNodeResetButton->setEnabled(false);
}

void SettingsWindow::setTargetNode(const uint& x, const uint& y)
{
  ui->infoMessagesTextEdit->append("Setting target node:");
  targetNodeX = x;
  ui->infoMessagesTextEdit->append(" => Target node X coordinate successfully set to " + QString::number(targetNodeX));
  targetNodeY = y;
  ui->infoMessagesTextEdit->append(" => Target node Y coordinate successfully set to " + QString::number(targetNodeY));

  graphicalTargetNode = graphicsScene->addEllipse(targetNodeX, targetNodeY, NODE_RADIUS, NODE_RADIUS, QPen(Qt::red), QBrush(Qt::red));

  ui->targetNodeSetButton->setEnabled(false);
  ui->targetNodeResetButton->setEnabled(true);
}

void SettingsWindow::resetTargetNode()
{
  ui->infoMessagesTextEdit->append("Target node coordinates successfully reset.");

  targetNodeX = 0;
  targetNodeY = 0;

  delete graphicalTargetNode;

  ui->targetNodeSetButton->setEnabled(true);
  ui->targetNodeResetButton->setEnabled(false);
}

void SettingsWindow::setBuildTargetSubtree(const bool& buildTargetSubtree)
{
  this->buildTargetSubtree = buildTargetSubtree;
  ui->infoMessagesTextEdit->append("Build target subtree successfully set to " + QString::number((int) buildTargetSubtree));
}

void SettingsWindow::setMaxNodesNumber(const uint& maxNodesNumber)
{
  this->maxNodesNumber = maxNodesNumber;
  ui->infoMessagesTextEdit->append("Maximum number of nodes successfully set to " + QString::number(maxNodesNumber));
}

void SettingsWindow::setMaxStepSize(const double& maxStepSize)
{
  this->maxStepSize = maxStepSize;
  ui->infoMessagesTextEdit->append("Maximum step size successfully set to " + QString::number(maxStepSize));
}

void SettingsWindow::setRRTUpdateInterval(const uint& updateInterval)
{
  RRTUpdateInterval = updateInterval;
  ui->infoMessagesTextEdit->append("RRT update interval successfully set to " + QString::number(RRTUpdateInterval));
}

void SettingsWindow::setMaxNeighboursRadius(const double& maxNeighboursRadius)
{
  this->maxNeighboursRadius = maxNeighboursRadius;
  ui->infoMessagesTextEdit->append("Maximum radius of neighbours successfully set to " + QString::number(maxNeighboursRadius));
}

void SettingsWindow::setRRTStarUpdateInterval(const uint& updateInterval)
{
  RRTStarUpdateInterval = updateInterval;
  ui->infoMessagesTextEdit->append("RRT* update interval successfully set to " + QString::number(RRTStarUpdateInterval));
}

void SettingsWindow::setTreeRedrawInterval(const uint& treeRedrawInterval)
{
  this->treeRedrawInterval = treeRedrawInterval;
  ui->infoMessagesTextEdit->append("RRT* tree redraw interval successfully set to " + QString::number(treeRedrawInterval));
}

void SettingsWindow::setDijkstraUpdateInterval(const uint& DijkstraUpdateInterval)
{
  this->DijkstraUpdateInterval = DijkstraUpdateInterval;
  ui->infoMessagesTextEdit->append("Dijkstra update interval successfully set to " + QString::number(DijkstraUpdateInterval));
}

void SettingsWindow::setDrawQuadTreesBoundaries(const bool& drawQuadTreesBoundaries)
{
  this->drawQuadtreesBoundaries = drawQuadTreesBoundaries;
  ui->infoMessagesTextEdit->append("Draw quadtrees boundaries successfully set to " + QString::number(drawQuadTreesBoundaries));

  if (drawQuadTreesBoundaries)
    ui->drawQuadtreesBoundariesCheckBox->setChecked(true);
  else
    ui->drawQuadtreesBoundariesCheckBox->setChecked(false);
}

void SettingsWindow::setObstacles(const QVector<QPolygon>& obstacles)
{
  this->obstacles = obstacles;
  ui->infoMessagesTextEdit->append("Found " + QString::number(obstacles.size()) + " obstacle" + (obstacles.size() > 1 ? "s." : "."));

  for (int i = 0; i < obstacles.size(); i++)
  {
    QColor border(Qt::black);
    QColor inside(Qt::gray);
    graphicalObstacles.push_back(graphicsScene->addPolygon(obstacles[i], QPen(border), QBrush(inside)));
    obstaclesItemModel.appendRow(new QStandardItem(polygon_to_string(obstacles.at(i))));
  }

  if (obstacles.size() > 0)
  {
    ui->clearAllObstaclesButton->setEnabled(true);
    ui->clearObstacleButton->setEnabled(true);
  }
}

void SettingsWindow::setOffLimitsZones(const QVector<QPolygon>& offLimitsZones)
{
  this->offLimitsZones = offLimitsZones;
  ui->infoMessagesTextEdit->append("Found " + QString::number(offLimitsZones.size()) + " off limits zone" + (obstacles.size() > 1 ? "s." : "."));

  for (int i = 0; i < offLimitsZones.size(); i++)
  {
    QColor border(Qt::black);
    QColor inside(Qt::red);
    graphicalOffLimitsZones.push_back(graphicsScene->addPolygon(offLimitsZones[i], QPen(border), QBrush(inside)));
    offLimitsZonesItemModel.appendRow(new QStandardItem(polygon_to_string(offLimitsZones.at(i))));
  }

  if (offLimitsZones.size() > 0)
  {
    ui->clearAllOffLimitsZonesButton->setEnabled(true);
    ui->clearOffLimitsZoneButton->setEnabled(true);
  }
}

void SettingsWindow::setPreferredZones(const QVector<QPolygon>& preferredZones)
{
  this->preferredZones = preferredZones;
  ui->infoMessagesTextEdit->append("Found " + QString::number(preferredZones.size()) + " preferred zone" + (obstacles.size() > 1 ? "s." : "."));

  for (int i = 0; i < preferredZones.size(); i++)
  {
    QColor border(Qt::black);
    QColor inside(Qt::green);
    graphicalPreferredZones.push_back(graphicsScene->addPolygon(preferredZones[i], QPen(border), QBrush(inside)));
    preferredZonesItemModel.appendRow(new QStandardItem(polygon_to_string(preferredZones.at(i))));
  }

  if (preferredZones.size() > 0)
  {
    ui->clearAllPreferredZonesButton->setEnabled(true);
    ui->clearPreferredZoneButton->setEnabled(true);
  }
}

void SettingsWindow::setUIFields()
{
  ui->mapWidthSpinBox->setValue(mapWidth);
  ui->mapHeightSpinBox->setValue(mapHeight);

  ui->startingNodeXSpinBox->setValue(startingNodeX);
  ui->startingNodeYSpinBox->setValue(startingNodeY);

  ui->targetNodeXSpinBox->setValue(targetNodeX);
  ui->targetNodeYSpinBox->setValue(targetNodeY);

  ui->buildTargetSubtreeCheckbox->setChecked(buildTargetSubtree);
  ui->maxNodesNumberSpinBox->setValue(maxNodesNumber);
  ui->maxStepSizeDoubleSpinBox->setValue(maxStepSize);
  ui->RRTUpdateIntervalSpinBox->setValue(RRTUpdateInterval);

  ui->maxNeighboursRadiusDoubleSpinBox->setValue(maxNeighboursRadius);
  ui->RRTStarUpdateIntervalSpinBox->setValue(RRTStarUpdateInterval);
  ui->treeRedrawIntervalSpinBox->setValue(treeRedrawInterval);

  ui->DijkstraUpdateIntervalSpinBox->setValue(DijkstraUpdateInterval);
  ui->drawQuadtreesBoundariesCheckBox->setChecked(drawQuadtreesBoundaries);
}

// FIELD MODIFIERS ======================================================================================
void SettingsWindow::on_mapSizeSetButton_clicked()
{
  uint map_width = (uint) ui->mapWidthSpinBox->value(), map_height = (uint) ui->mapHeightSpinBox->value();
  if (this->mapWidth != map_width || this->mapHeight != map_height)
    unsavedChanges[0] = true;

  setMapSize(ui->mapWidthSpinBox->value(), ui->mapHeightSpinBox->value());

  ui->mapSizeSetButton->setEnabled(false);
  ui->mapSizeResetButton->setEnabled(true);

  /* If the new map size still contains starting and target points and obstacles, they don't get touched */
  QPolygon map (QRect(0, 0, mapWidth + 1, mapHeight + 1));
  if (!map.containsPoint(QPoint(startingNodeX, startingNodeY), Qt::OddEvenFill))
  {
    unsavedChanges[1] = true;
    resetStartingNode();
  }

  if (!map.containsPoint(QPoint(targetNodeX, targetNodeY), Qt::OddEvenFill))
  {
    unsavedChanges[2] = true;
    resetTargetNode();
  }

  QVector<QPolygon> tmpObstacles, tmpOffLimitsZones;
  foreach(QPolygon obstacle, obstacles)
    if (QRect(0, 0, mapWidth, mapHeight).contains(obstacle.boundingRect()))
      tmpObstacles.push_back(obstacle);
  foreach(QPolygon offLimitsZone, offLimitsZones)
    if (QRect(0, 0, mapWidth, mapHeight).contains(offLimitsZone.boundingRect()))
      tmpOffLimitsZones.push_back(offLimitsZone);

  if (tmpObstacles != obstacles)
    unsavedChanges[3] = true;
  if (tmpOffLimitsZones != offLimitsZones)
    unsavedChanges[5] = true;

  on_clearAllObstaclesButton_clicked();
  setObstacles(tmpObstacles);

  on_clearAllOffLimitsZonesButton_clicked();
  setOffLimitsZones(tmpOffLimitsZones);
}

void SettingsWindow::on_mapSizeResetButton_clicked()
{
  unsavedChanges[0] = true;
  resetMapSize();
}

void SettingsWindow::on_startingNodeSetButton_clicked()
{
  uint x = ui->startingNodeXSpinBox->value();
  uint y = ui->startingNodeYSpinBox->value();

  QPoint p(x, y);
  bool inside = false;
  for (int i = 0; i < obstacles.size() && !inside; ++i)
    if (obstacles[i].containsPoint(p, Qt::OddEvenFill))
      inside = true;

  if (!inside)
  {
    if (startingNodeX != x || startingNodeY != y)
      unsavedChanges[1] = true;

    setStartingNode(x, y);
  }
  else
    ui->infoMessagesTextEdit->append("Starting node is valid but is inside an obstacle.\n==>Keeping old values (" + QString::number(startingNodeX) + "; " + QString::number(startingNodeY) + ")");
}

void SettingsWindow::on_startingNodeResetButton_clicked()
{
  unsavedChanges[1] = true;
  resetStartingNode();
}

void SettingsWindow::on_targetNodeSetButton_clicked()
{
  uint x = ui->targetNodeXSpinBox->value();
  uint y = ui->targetNodeYSpinBox->value();

  QPoint p(x, y);
  bool inside = false;
  for (int i = 0; i < obstacles.size() && !inside; ++i)
    if (obstacles[i].containsPoint(p, Qt::OddEvenFill))
      inside = true;

  if (!inside)
  {
    if (targetNodeX != x || targetNodeY != y)
      unsavedChanges[2] = true;

    setTargetNode(x, y);
  }
  else
    ui->infoMessagesTextEdit->append("Target node is valid but is inside an obstacle.\n==>Keeping old values (" + QString::number(targetNodeX) + "; " + QString::number(targetNodeY) + ")");
}

void SettingsWindow::on_targetNodeResetButton_clicked()
{
  unsavedChanges[2] = true;
  resetTargetNode();
}

void SettingsWindow::on_addObstacleButton_clicked()
{
  ui->addObstacleButton->setEnabled(false);
  ui->graphicsView->setEnabled(true);
}

void SettingsWindow::on_drawObstacleButton_clicked()
{
  QPolygon obstacle(obstacleVertices);

  if (obstacle.containsPoint(QPoint(startingNodeX, startingNodeY), Qt::OddEvenFill) ||
      obstacle.containsPoint(QPoint(targetNodeX, targetNodeY), Qt::OddEvenFill))
  {
    ui->statusBar->showMessage(QString("The obstacle you are trying to add contains starting point or target point. Discarding..."));
  }
  else
  {
    graphicalObstacles.push_back(graphicsScene->addPolygon(obstacle, QPen(Qt::black), QBrush(Qt::gray)));
    obstacles.push_back(obstacle);

    QString tmp = polygon_to_string(obstacle);
    ui->infoMessagesTextEdit->append("Added new obstacle: " + tmp);
    obstaclesItemModel.appendRow(new QStandardItem(tmp));

    ui->clearAllObstaclesButton->setEnabled(true);
    ui->clearObstacleButton->setEnabled(true);

    unsavedChanges[3] = true;
  }
  ui->drawObstacleButton->setEnabled(false);
  ui->addObstacleButton->setEnabled(true);

  startedAddingObstacleVertices = false;
  for (int i = 0; i < graphicalObstacleVertices.size(); i++)
    delete graphicalObstacleVertices[i];
  graphicalObstacleVertices.clear();
  obstacleVertices.clear();
}

void SettingsWindow::on_clearObstacleButton_clicked()
{
  int index = ui->obstaclesListView->currentIndex().row();

  if (index >= 0 && index < obstacles.size())
  {
    ui->infoMessagesTextEdit->append("Polygon " + polygon_to_string(obstacles[index]) + " removed.");
    delete graphicalObstacles[index];
    graphicalObstacles.removeAt(index);
    obstacles.removeAt(index);
    obstaclesItemModel.removeRow(index);
    unsavedChanges[3] = true;
  }
  else
    ui->statusBar->showMessage("Please select something first.");

  if (obstacles.size() < 1)
  {
    ui->clearAllObstaclesButton->setEnabled(false);
    ui->clearObstacleButton->setEnabled(false);
  }
}

void SettingsWindow::on_clearAllObstaclesButton_clicked()
{
  ui->infoMessagesTextEdit->append("Erasing obstacles...");
  obstaclesItemModel.clear();

  obstacles.clear();
  for (int i = 0; i < graphicalObstacles.size(); i++)
    delete graphicalObstacles.at(i);
  graphicalObstacles.clear();

  obstacleVertices.clear();
  for (int i = 0; i < graphicalObstacleVertices.size(); i++)
    delete graphicalObstacleVertices.at(i);
  graphicalObstacleVertices.clear();

  ui->addObstacleButton->setEnabled(true);
  ui->drawObstacleButton->setEnabled(false);
  ui->clearObstacleButton->setEnabled(false);
  ui->clearAllObstaclesButton->setEnabled(false);

  ui->infoMessagesTextEdit->append("All obstacles erased.");

  obstaclesTempRow.clear();
  unsavedChanges[3] = true;
}

void SettingsWindow::on_addPreferredZoneButton_clicked()
{
  ui->addPreferredZoneButton->setEnabled(false);
  ui->graphicsView->setEnabled(true);
}

void SettingsWindow::on_drawPreferredZoneButton_clicked()
{
  QPolygon preferredZone(preferredZoneVertices);

  graphicalPreferredZones.push_back(graphicsScene->addPolygon(preferredZone, QPen(Qt::black), QBrush(Qt::green)));
  preferredZones.push_back(preferredZone);

  QString tmp = polygon_to_string(preferredZone);
  ui->infoMessagesTextEdit->append("Added new preferred zone: " + tmp);
  preferredZonesItemModel.appendRow(new QStandardItem(tmp));

  ui->clearAllPreferredZonesButton->setEnabled(true);
  ui->clearPreferredZoneButton->setEnabled(true);

  unsavedChanges[4] = true;

  ui->drawPreferredZoneButton->setEnabled(false);
  ui->addPreferredZoneButton->setEnabled(true);

  startedAddingPreferredZoneVertices = false;
  for (int i = 0; i < graphicalPreferredZoneVertices.size(); i++)
    delete graphicalPreferredZoneVertices[i];
  graphicalPreferredZoneVertices.clear();
  preferredZoneVertices.clear();
}

void SettingsWindow::on_clearPreferredZoneButton_clicked()
{
  int index = ui->preferredZonesListView->currentIndex().row();

  if (index >= 0 && index < preferredZones.size())
  {
    ui->infoMessagesTextEdit->append("Preferred zone " + polygon_to_string(preferredZones[index]) + " removed.");
    delete graphicalPreferredZones[index];
    graphicalPreferredZones.removeAt(index);
    preferredZones.removeAt(index);
    preferredZonesItemModel.removeRow(index);
    unsavedChanges[4] = true;
  }
  else
    ui->statusBar->showMessage("Please select something first.");

  if (preferredZones.size() < 1)
  {
    ui->clearAllPreferredZonesButton->setEnabled(false);
    ui->clearPreferredZoneButton->setEnabled(false);
  }
}

void SettingsWindow::on_clearAllPreferredZonesButton_clicked()
{
  ui->infoMessagesTextEdit->append("Erasing preferred zones...");
  preferredZonesItemModel.clear();

  preferredZones.clear();
  for (int i = 0; i < graphicalPreferredZones.size(); i++)
    delete graphicalPreferredZones.at(i);
  graphicalPreferredZones.clear();

  preferredZoneVertices.clear();
  for (int i = 0; i < graphicalPreferredZoneVertices.size(); i++)
    delete graphicalPreferredZoneVertices.at(i);
  graphicalPreferredZoneVertices.clear();

  ui->addPreferredZoneButton->setEnabled(true);
  ui->drawPreferredZoneButton->setEnabled(false);
  ui->clearPreferredZoneButton->setEnabled(false);
  ui->clearAllPreferredZonesButton->setEnabled(false);

  ui->infoMessagesTextEdit->append("All preferred zones erased.");

  preferredZonesTempRow.clear();
  unsavedChanges[4] = true;
}

void SettingsWindow::on_addOffLimitsZoneButton_clicked()
{
  ui->addOffLimitsZoneButton->setEnabled(false);
  ui->graphicsView->setEnabled(true);
}

void SettingsWindow::on_drawOffLimitsZoneButton_clicked()
{
  QPolygon offLimitsZone(offLimitsZoneVertices);

  if (offLimitsZone.containsPoint(QPoint(startingNodeX, startingNodeY), Qt::OddEvenFill) ||
      offLimitsZone.containsPoint(QPoint(targetNodeX, targetNodeY), Qt::OddEvenFill))
  {
    ui->statusBar->showMessage(QString("The off limits zone you are trying to add contains starting point or target point. Discarding..."));
    offLimitsZonesItemModel.removeRow(offLimitsZonesItemModel.rowCount() - 1);
  }
  else
  {
    graphicalOffLimitsZones.push_back(graphicsScene->addPolygon(offLimitsZone, QPen(Qt::black), QBrush(Qt::red)));
    offLimitsZones.push_back(offLimitsZone);

    QString tmp = polygon_to_string(offLimitsZone);
    ui->infoMessagesTextEdit->append("Added new off limits zone: " + tmp);
    offLimitsZonesItemModel.appendRow(new QStandardItem(tmp));

    ui->clearAllOffLimitsZonesButton->setEnabled(true);
    ui->clearOffLimitsZoneButton->setEnabled(true);

    unsavedChanges[5] = true;
  }

  ui->drawOffLimitsZoneButton->setEnabled(false);
  ui->addOffLimitsZoneButton->setEnabled(true);

  startedAddingOffLimitsZoneVertices = false;
  for (int i = 0; i < graphicalOffLimitsZoneVertices.size(); i++)
    delete graphicalOffLimitsZoneVertices[i];
  graphicalOffLimitsZoneVertices.clear();
  offLimitsZoneVertices.clear();
}

void SettingsWindow::on_clearOffLimitsZoneButton_clicked()
{
  int index = ui->offLimitsZonesListView->currentIndex().row();

  if (index >= 0 && index < offLimitsZones.size())
  {
    ui->infoMessagesTextEdit->append("Off limits zone " + polygon_to_string(offLimitsZones[index]) + " removed.");
    delete graphicalOffLimitsZones[index];
    graphicalOffLimitsZones.removeAt(index);
    offLimitsZones.removeAt(index);
    offLimitsZonesItemModel.removeRow(index);
    unsavedChanges[5] = true;
  }
  else
    ui->statusBar->showMessage("Please select an off limits zone first.");

  if (offLimitsZones.size() < 1)
  {
    ui->clearAllOffLimitsZonesButton->setEnabled(false);
    ui->clearOffLimitsZoneButton->setEnabled(false);
  }
}

void SettingsWindow::on_clearAllOffLimitsZonesButton_clicked()
{
  ui->infoMessagesTextEdit->append("Erasing off limits zones...");
  offLimitsZonesItemModel.clear();

  offLimitsZones.clear();
  for (int i = 0; i < graphicalOffLimitsZones.size(); i++)
    delete graphicalOffLimitsZones.at(i);
  graphicalOffLimitsZones.clear();

  offLimitsZoneVertices.clear();
  for (int i = 0; i < graphicalOffLimitsZoneVertices.size(); i++)
    delete graphicalOffLimitsZoneVertices.at(i);
  graphicalOffLimitsZoneVertices.clear();

  ui->addOffLimitsZoneButton->setEnabled(true);
  ui->drawOffLimitsZoneButton->setEnabled(false);
  ui->clearOffLimitsZoneButton->setEnabled(false);
  ui->clearAllOffLimitsZonesButton->setEnabled(false);

  ui->infoMessagesTextEdit->append("All off limits zones erased.");

  offLimitsZonesTempRow.clear();
  unsavedChanges[5] = true;
}

void SettingsWindow::on_maxNodesNumberSpinBox_editingFinished()
{
  if (maxNodesNumber != (uint) ui->maxNodesNumberSpinBox->value())
  {
    unsavedChanges[6] = true;
    setMaxNodesNumber(ui->maxNodesNumberSpinBox->value());
  }
}

void SettingsWindow::on_maxStepSizeDoubleSpinBox_editingFinished()
{
  if (maxStepSize != ui->maxStepSizeDoubleSpinBox->value())
  {
    unsavedChanges[7] = true;
    setMaxStepSize(ui->maxStepSizeDoubleSpinBox->value());
  }
}

void SettingsWindow::on_RRTUpdateIntervalSpinBox_editingFinished()
{
  if (RRTUpdateInterval != (uint) ui->RRTUpdateIntervalSpinBox->value())
  {
    unsavedChanges[8] = true;
    setRRTUpdateInterval(ui->RRTUpdateIntervalSpinBox->value());
  }
}

void SettingsWindow::on_buildTargetSubtreeCheckbox_clicked()
{
  if (buildTargetSubtree != ui->buildTargetSubtreeCheckbox->isChecked())
  {
    unsavedChanges[9] = !unsavedChanges[9];
    setBuildTargetSubtree(ui->buildTargetSubtreeCheckbox->isChecked());
  }
}

void SettingsWindow::on_startRRTButton_clicked()
{
  maybeSave();
  QProcess* p = new QProcess(this);
  RRTProcesses.push_back(p);
  p->start("../../RRT/RRT", QStringList() << "--file" << "../../settings.conf");
}

void SettingsWindow::on_maxNeighboursRadiusDoubleSpinBox_editingFinished()
{
  if (maxNeighboursRadius != ui->maxNeighboursRadiusDoubleSpinBox->value())
  {
    unsavedChanges[10] = true;
    setMaxNeighboursRadius(ui->maxNeighboursRadiusDoubleSpinBox->value());
  }
}

void SettingsWindow::on_RRTStarUpdateIntervalSpinBox_editingFinished()
{
  if (RRTStarUpdateInterval != (uint) ui->RRTStarUpdateIntervalSpinBox->value())
  {
    unsavedChanges[11] = true;
    setRRTStarUpdateInterval(ui->RRTStarUpdateIntervalSpinBox->value());
  }
}

void SettingsWindow::on_treeRedrawIntervalSpinBox_editingFinished()
{
  if (treeRedrawInterval != (uint) ui->treeRedrawIntervalSpinBox->value())
  {
    unsavedChanges[13] = true;
    setTreeRedrawInterval(ui->treeRedrawIntervalSpinBox->value());
  }
}

void SettingsWindow::on_startRRTStarButton_clicked()
{
  maybeSave();
  QProcess* p = new QProcess(this);
  RRTStarProcesses.push_back(p);
  p->start("../../RRTStar/RRTStar", QStringList() << "--file" << "../../settings.conf");
}

void SettingsWindow::on_DijkstraUpdateIntervalSpinBox_editingFinished()
{
  if (DijkstraUpdateInterval != (uint) ui->DijkstraUpdateIntervalSpinBox->value())
  {
    unsavedChanges[14] = true;
    setDijkstraUpdateInterval(ui->DijkstraUpdateIntervalSpinBox->value());
  }
}

void SettingsWindow::on_drawQuadtreesBoundariesCheckBox_clicked()
{
  if (drawQuadtreesBoundaries != ui->drawQuadtreesBoundariesCheckBox->isChecked())
  {
    unsavedChanges[15] = !unsavedChanges[15];
    setDrawQuadTreesBoundaries(ui->drawQuadtreesBoundariesCheckBox->isChecked());
  }
}

void SettingsWindow::on_startDijkstraButton_clicked()
{
  maybeSave();
  QProcess* p = new QProcess(this);
  DijkstraProcesses.push_back(p);
  p->start("../../Dijkstra/Dijkstra", QStringList() << "--file" << "../../settings.conf");
}

//=MENU=========================================================================================================================
void SettingsWindow::on_actionLoad_triggered()
{
  maybeSave();
  QFileInfo fi;
  QString file_name = QFileDialog::getOpenFileName(this,
                                                   "Load configuration file...",
                                                   fi.absolutePath() + "../../",
                                                   tr("Configuration files (*.conf) (*.conf)"));
  loadFile(file_name);
}

void SettingsWindow::loadFile(const QString& fileName)
{
  graphicsScene->clear();

  this->obstaclesItemModel.clear();
  this->preferredZonesItemModel.clear();
  this->offLimitsZonesItemModel.clear();

  if (!fileName.isEmpty())
  {
    FileParser parser(fileName);
    setCurrentConfigurationFile(parser.getConfigFile());
    setMapSize(parser.getMapWidth(), parser.getMapHeight());
    setStartingNode(parser.getStartingNodeX(), parser.getStartingNodeY());
    setTargetNode(parser.getTargetNodeX(), parser.getTargetNodeY());
    setBuildTargetSubtree(parser.getBuildTargetSubtree());
    setMaxNodesNumber(parser.getMaxNodesNumber());
    setMaxStepSize(parser.getMaxStepSize());
    setRRTUpdateInterval(parser.getRRTUpdateInterval());
    setMaxNeighboursRadius(parser.getMaxNeighbourRadius());
    setRRTStarUpdateInterval(parser.getRRTStarUpdateInterval());
    setTreeRedrawInterval(parser.getTreeRedrawInterval());
    setObstacles(parser.getObstacles());
    setPreferredZones(parser.getPreferredZones());
    setOffLimitsZones(parser.getOffLimitsZones());
    setDijkstraUpdateInterval(parser.getDijkstraUpdateInterval());
    setDrawQuadTreesBoundaries(parser.getDrawQuadTreesBoundaries());
    setUIFields();

    unsavedChanges.fill(false);
  }
}

void SettingsWindow::on_actionSave_triggered()
{
  if (!currentConfigurationFile.isEmpty())
    saveFile(currentConfigurationFile);
  else
    saveFile(DEFAULT_CONF_FILE);
}

void SettingsWindow::on_actionSave_as_triggered()
{
  QFileDialog dialog(this);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  QStringList files;

  if (dialog.exec())
    files = dialog.selectedFiles();

  saveFile(files.at(0));
}

void SettingsWindow::saveFile(const QString& fileName)
{
  std::ofstream ofs(fileName.toStdString().c_str(), std::ios::trunc);

  if (!ofs)
  {
    QMessageBox::warning(this, tr("Save file"),
                         tr("Cannot write file %1:\n%2.")
                         .arg(fileName)
                         .arg(QString::fromLocal8Bit(strerror(errno))));
    std::cerr << "Cannot write file " << fileName.toStdString() << ": " << strerror(errno) << "\n\n";
  }
  else
  {
    ofs << "# Map size section\n"
        << "[MAP]\n"
        << "  WIDTH = " << mapWidth << "\n"
        << "  HEIGHT = " << mapHeight << "\n"
        << "\n"
        << "# Starting point section\n"
        << "[STARTING NODE]\n"
        << "  X = " << startingNodeX << "\n"
        << "  Y = " << startingNodeY << "\n"
        << "\n"
        << "# Target point section\n"
        << "[TARGET NODE]\n"
        << "  X = " << targetNodeX << "\n"
        << "  Y = " << targetNodeY << "\n"
        << "\n"
        << "# RRT settings\n"
        << "[RRT]\n"
        << "  BUILD_TARGET_NODE_SUBTREE = " << buildTargetSubtree << "\n"
        << "  MAX_NODES_NUMBER = " << maxNodesNumber << "\n"
        << "  MAX_STEP_SIZE = " << maxStepSize << "\n"
        << "  UPDATE_INTERVAL = " << RRTUpdateInterval << "\n"
        << "\n"
        << "# RRT* settings\n"
        << "[RRT*]\n"
        << "  MAX_NEIGHBOURS_RADIUS = " << maxNeighboursRadius << "\n"
        << "  UPDATE_INTERVAL = " << RRTStarUpdateInterval << "\n"
        << "  TREE_REDRAW_INTERVAL = " << treeRedrawInterval << "\n"
        << "\n"
        << "# Dijkstra settings\n"
        << "[DIJKSTRA]\n"
        << "  UPDATE_INTERVAL = " << DijkstraUpdateInterval << "\n"
        << "  DRAW_QUADTREES_BOUNDARIES = " << drawQuadtreesBoundaries << "\n";

    uint numberOfZones, zoneSpaces, numberOfVertices, verticesSpaces;

    numberOfZones = obstacles.size();
    zoneSpaces = 1;
    while (numberOfZones /= 10)
      zoneSpaces++;
    for (int i = 0; i < obstacles.size(); ++i)
    {
      ofs << "\n# " << i + 1 << printOrdinalSuffix(i) << " obstacle"
          << "\n[OBSTACLE_" << std::setfill('0') << std::setw(zoneSpaces) << i + 1 << "]\n";

      numberOfVertices = obstacles[i].size();
      verticesSpaces = 1;
      while (numberOfVertices /= 10)
        verticesSpaces++;

      for (int j = 0; j < obstacles[i].size(); ++j)
      {
        ofs << "  # " << j + 1 << printOrdinalSuffix(j) << " vertex\n"
            << "  X" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << obstacles[i][j].x() << "\n"
            << "  Y" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << obstacles[i][j].y() << "\n";
      }
    }

    numberOfZones = preferredZones.size();
    zoneSpaces = 1;
    while (numberOfZones /= 10)
      zoneSpaces++;
    for (int i = 0; i < preferredZones.size(); ++i)
    {
      ofs << "\n# " << i + 1 << printOrdinalSuffix(i) << " preferred zones"
          << "\n[PREFERRED_ZONE_" << std::setfill('0') << std::setw(zoneSpaces) << i + 1 << "]\n";

      numberOfVertices = preferredZones[i].size();
      verticesSpaces = 1;
      while (numberOfVertices /= 10)
        verticesSpaces++;
      for (int j = 0; j < preferredZones[i].size(); ++j)
      {
        ofs << "  # " << j + 1 << printOrdinalSuffix(j) << " vertex\n"
            << "  X" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << preferredZones[i][j].x() << "\n"
            << "  Y" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << preferredZones[i][j].y() << "\n";
      }
    }

    numberOfZones = offLimitsZones.size();
    zoneSpaces = 1;
    while (numberOfZones /= 10)
      zoneSpaces++;
    for (int i = 0; i < offLimitsZones.size(); ++i)
    {
      ofs << "\n# " << i + 1 << printOrdinalSuffix(i) << " off limits zone"
          << "\n[OFF_LIMITS_ZONE_" << std::setfill('0') << std::setw(zoneSpaces) << i + 1 << "]\n";

      numberOfVertices = offLimitsZones[i].size();
      verticesSpaces = 1;
      while (numberOfVertices /= 10)
        verticesSpaces++;
      for (int j = 0; j < offLimitsZones[i].size(); ++j)
      {
        ofs << "  # " << j + 1 << printOrdinalSuffix(j) << " vertex\n"
            << "  X" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << offLimitsZones[i][j].x() << "\n"
            << "  Y" << std::setfill('0') << std::setw(verticesSpaces) << j + 1 << " = " << offLimitsZones[i][j].y() << "\n";
      }
    }
  }

  ui->statusBar->showMessage("File " + fileName + " saved.", 2000);
  unsavedChanges.fill(false);
}

void SettingsWindow::maybeSave()
{
  if (QVector<bool>(16, false) != unsavedChanges)
  {
    QMessageBox about_to_save(QMessageBox::Warning,
                              "Unsaved changes detected",
                              "Warning! There are unsaved changes.\nWhat do you want to do?",
                              QMessageBox::Save | QMessageBox::Cancel);
    if (about_to_save.exec() == QMessageBox::Save)
      saveFile(currentConfigurationFile);
  }
}

void SettingsWindow::printMouseCoordinates(const qreal& x, const qreal& y)
{
  ui->statusBar->showMessage(QString("Mouse (") + QString::number(x) + QString(", ") + QString::number(y) + QString(")"));
}

void SettingsWindow::drawPoint(const qreal& x, const qreal& y)
{
  if (!ui->addObstacleButton->isEnabled())
  {
    graphicalObstacleVertices.push_back(graphicsScene->addEllipse(x, y, 1.0, 1.0));
    obstacleVertices.push_back(QPoint(x, y));
    if (obstacleVertices.size() > 2)
      ui->drawObstacleButton->setEnabled(true);
  }
  else if (!ui->addPreferredZoneButton->isEnabled())
  {
    graphicalPreferredZoneVertices.push_back(graphicsScene->addEllipse(x, y, 1.0, 1.0));
    preferredZoneVertices.push_back(QPoint(x, y));
    if (preferredZoneVertices.size() > 2)
      ui->drawPreferredZoneButton->setEnabled(true);
  }
  else if (!ui->addOffLimitsZoneButton->isEnabled())
  {
    graphicalOffLimitsZoneVertices.push_back(graphicsScene->addEllipse(x, y, 1.0, 1.0));
    offLimitsZoneVertices.push_back(QPoint(x, y));
    if (offLimitsZoneVertices.size() > 2)
      ui->drawOffLimitsZoneButton->setEnabled(true);
  }
}

const char* SettingsWindow::printOrdinalSuffix(const uint& i)
{
  if ((i == 12 || ((i - 12) % 100 == 0)) ||
      (i == 11 || ((i - 11) % 100 == 0)) ||
      (i == 10 || ((i - 10) % 100 == 0)))
    return "th";
  else if (i == 0 || (i % 10 == 0))
    return "st";
  else if (i == 1 || ((i - 1) % 10 == 0))
    return "nd";
  else if (i == 2 || ((i - 2) % 10 == 0))
    return "rd";
  else
    return "th";
}

void SettingsWindow::on_actionExit_triggered()
{
  maybeSave();

  qApp->quit();
}

void SettingsWindow::on_actionAbout_triggered()
{
  QMessageBox about(QMessageBox::Information,
                    "About",
                    "This is a utility to build a simple configuration file.",
                    QMessageBox::Ok);
  about.exec();
}

void SettingsWindow::closeEvent(QCloseEvent* closeEvent)
{
  maybeSave();
  closeEvent->accept();
}
