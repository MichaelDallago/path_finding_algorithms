#-------------------------------------------------
#
# Project created by QtCreator 2015-11-27T21:03:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SettingsWindow
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    ../FileParser/file_parser.cpp \
    ../ZoomableGraphicsView/zoomablegraphicsview.cpp

HEADERS  += mainwindow.h \
    ../FileParser/file_parser.h \
    ../ZoomableGraphicsView/zoomablegraphicsview.h

FORMS += mainwindow.ui

CONFIG += c++11

DISTFILES += \
    ../../Debug/settings.conf
