#include "mainwindow.h"
#include <QApplication>

#include <istream>
#include <iostream>
#include <QCommandLineParser>

void showVersion()
{
  std::cout << "SettingsWindow for planning algorithm, version 1.1" << "\n";
  std::cout << "Changelog:\n"
            << "v1.1:\n"
            << " - Configuration file is automatically created if it does not exist.\n\n"
            << "v1.0:\n"
            << " - Removed boost libraried dependencies.\n"
            << " - Using QCommandLineParser to parse command line options.\n"
            << " - Switched from Qt::QGraphicsView to ZoomableGraphicsView.\n\n"
            << "v0.2.1:\n"
            << " - Fixed bug when clicking in listView while adding obstacle\n\n"
            << "v0.2:\n"
            << " - Additional info displayed in config file (vertex and obstacle number)\n"
               " for better understanding of possible errors returned by the file parser\n\n"
            << "v0.1:\n"
            << " - Initial command line options setup\n "
            << std::endl;
}

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  QCommandLineParser commandLineParser;
  commandLineParser.setApplicationDescription("Settings window");

  QCommandLineOption configurationFileOption(QStringList() << "f" << "file", "The configuration file to parse.", "conf_file");
  configurationFileOption.setDefaultValue("../../settings.conf");
  commandLineParser.addOption(configurationFileOption);
  const QCommandLineOption helpOption = commandLineParser.addHelpOption();
  const QCommandLineOption versionOption = commandLineParser.addVersionOption();

  if (!commandLineParser.parse(app.arguments()))
  {
    std::cerr << "Bad command line option: " << commandLineParser.errorText().toStdString() << "\n";
    commandLineParser.showHelp();
    return 0;
  }

  if (commandLineParser.isSet(versionOption))
  {
    showVersion();
    return 0;
  }

  if (commandLineParser.isSet(helpOption))
  {
    commandLineParser.showHelp();
    return 0;
  }

  const QString fileName = commandLineParser.value(configurationFileOption);

  QFile configFile(fileName);
  if (!configFile.exists())
  {
    std::cerr << "Warning: configuration file " << fileName.toStdString() << " does not exist.\n => Trying to create an empty one...\n";
    if (!configFile.open(QIODevice::ReadWrite))
    {
      std::cerr << " => Unable to create empty file " << fileName.toStdString() << ": " << strerror(errno) << ".\n\n";
      return EXIT_FAILURE;
    }
    else
    {
      std::cerr << " => Empty file " << fileName.toStdString() << " successfully created.\n\n";
      configFile.close();
    }
  }

  if (!configFile.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    std::cerr << "Unable to open configuration file " << fileName.toStdString() << ": " << strerror(errno) << ".\n\n";
    return EXIT_FAILURE;
  }
  else
    configFile.close();

  SettingsWindow w;
  FileParser fileParser(fileName);
  w.setCurrentConfigurationFile(fileName);
  w.setMapSize(fileParser.getMapWidth(), fileParser.getMapHeight());
  w.setStartingNode(fileParser.getStartingNodeX(), fileParser.getStartingNodeY());
  w.setTargetNode(fileParser.getTargetNodeX(), fileParser.getTargetNodeY());
  w.setBuildTargetSubtree(fileParser.getBuildTargetSubtree());
  w.setMaxNodesNumber(fileParser.getMaxNodesNumber());
  w.setMaxStepSize(fileParser.getMaxStepSize());
  w.setRRTUpdateInterval(fileParser.getRRTUpdateInterval());
  w.setMaxNeighboursRadius(fileParser.getMaxNeighbourRadius());
  w.setRRTStarUpdateInterval(fileParser.getRRTStarUpdateInterval());
  w.setTreeRedrawInterval(fileParser.getTreeRedrawInterval());
  w.setDijkstraUpdateInterval(fileParser.getDijkstraUpdateInterval());
  w.setDrawQuadTreesBoundaries(fileParser.getDrawQuadTreesBoundaries());
  w.setObstacles(fileParser.getObstacles());
  w.setOffLimitsZones(fileParser.getOffLimitsZones());
  w.setPreferredZones(fileParser.getPreferredZones());
  w.setUIFields();
  w.show();

  return app.exec();
}
