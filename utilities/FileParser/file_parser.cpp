#include "file_parser.h"
#include <iostream>

#define DEFAULT_MAP_WIDTH										200
#define DEFAULT_MAP_HEIGHT									200
#define DEFAULT_STARTING_NODE_X								0
#define DEFAULT_STARTING_NODE_Y								0
#define DEFAULT_TARGET_NODE_X								 10
#define DEFAULT_TARGET_NODE_Y								 10
#define DEFAULT_TARGET_SUBTREE								0
#define DEFAULT_MAX_NODES_NUMBER					10000
#define DEFAULT_MAX_STEP_SIZE							   10
#define DEFAULT_RRT_UPDATE_INTERVAL					 33
#define DEFAULT_MAX_NEIGHBOURS_RADIUS				 15
#define DEFAULT_RRTSTAR_UPDATE_INTERVAL			 33
#define DEFAULT_TREE_REDRAW_INTERVAL				300
#define DEFAULT_DIJKSTRA_UPDATE_INTERVAL     33
#define DEFAULT_DRAW_QUADTREES_BOUNDARIES     0

FileParser::FileParser(const QString& fileName) :
  settings (fileName, QSettings::NativeFormat)
{
  initVariables();

  if (settings.status() != 0)
    std::cerr << "Unable to open configuration file " << fileName.toStdString() << ".\n\n";
  else
  {
    setMapWidth(settings.value("MAP/WIDTH").toUInt());
    setMapHeight(settings.value("MAP/HEIGHT").toUInt());
    setStartingNodeX(settings.value("STARTING NODE/X", -1).toUInt());
    setStartingNodeY(settings.value("STARTING NODE/Y", -1).toUInt());
    setTargetNodeX(settings.value("TARGET NODE/X", -1).toUInt());
    setTargetNodeY(settings.value("TARGET NODE/Y", -1).toUInt());
    setBuildTargetNodeSubtree(settings.value("RRT/BUILD_TARGET_NODE_SUBTREE").toBool());
    setMaxNodesNumber(settings.value("RRT/MAX_NODES_NUMBER").toUInt());
    setMaxStepSize(settings.value("RRT/MAX_STEP_SIZE").toDouble());
    setRRTUpdateInterval(settings.value("RRT/UPDATE_INTERVAL").toUInt());
    setMaxNeighboursRadius(settings.value("RRT*/MAX_NEIGHBOURS_RADIUS").toDouble());
    setRRTStarUpdateInterval(settings.value("RRT*/UPDATE_INTERVAL").toUInt());
    setTreeRedrawInterval(settings.value("RRT*/TREE_REDRAW_INTERVAL").toUInt());
    setDijkstraUpdateInterval(settings.value("DIJKSTRA/UPDATE_INTERVAL").toUInt());
    setDrawQuadTreesBoundaries(settings.value("DIJKSTRA/DRAW_QUADTREES_BOUNDARIES").toBool());
    setZones();
  }
}

void FileParser::initVariables()
{
  mapWidth = DEFAULT_MAP_WIDTH, mapHeight = DEFAULT_MAP_HEIGHT;
  startingNodeX = DEFAULT_STARTING_NODE_X, startingNodeY = DEFAULT_STARTING_NODE_Y;
  targetNodeX = DEFAULT_TARGET_NODE_X, targetNodeY = DEFAULT_TARGET_NODE_Y;

  buildTargetSubtree = DEFAULT_TARGET_SUBTREE;
  maxNodesNumber = DEFAULT_MAX_NODES_NUMBER;
  maxStepSize = DEFAULT_MAX_STEP_SIZE;
  RRTUpdateInterval = DEFAULT_RRT_UPDATE_INTERVAL;

  maxNeighboursRadius = DEFAULT_MAX_NEIGHBOURS_RADIUS;
  RRTStarUpdateInterval = DEFAULT_RRTSTAR_UPDATE_INTERVAL;
  treeRedrawInterval = DEFAULT_TREE_REDRAW_INTERVAL;

  DijkstraUpdateInterval = DEFAULT_DIJKSTRA_UPDATE_INTERVAL;
  drawQuadTreesBoundaries = DEFAULT_DRAW_QUADTREES_BOUNDARIES;
}

void FileParser::validValue(const QString& validValueName, const QVariant& value)
{
  std::cerr << validValueName.toStdString() << " successfully set to "
            << value.toString().toStdString() << ".\n\n";
}

void FileParser::invalidValue(const QString& invalidValueName, const QVariant& defaultValue)
{
  std::cerr << "Invalid value for " << invalidValueName.toStdString()
            << " detected.\n => Setting it to default value "
            << defaultValue.toString().toStdString() << ".\n\n";
}

void FileParser::setMapWidth(const uint& mapWidth)
{
  if (mapWidth > 0)
  {
    validValue(QString("Map width"), mapWidth);
    this->mapWidth = mapWidth;
  }
  else
    invalidValue(QString("map width"), DEFAULT_MAP_WIDTH);
}

void FileParser::setMapHeight(const uint& mapHeight)
{
  if (mapHeight > 0)
  {
    validValue(QString("Map height"), mapHeight);
    this->mapHeight = mapHeight;
  }
  else
    invalidValue(QString("map height"), DEFAULT_MAP_HEIGHT);
}

void FileParser::setStartingNodeX(const uint& startingNodeX)
{
  if (startingNodeX <= mapWidth)
  {
    validValue(QString("Starting node X"), startingNodeX);
    this->startingNodeX = startingNodeX;
  }
  else
    invalidValue(QString("starting node X"), DEFAULT_STARTING_NODE_X);
}

void FileParser::setStartingNodeY(const uint& startingNodeY)
{
  if (startingNodeY <= mapHeight)
  {
    validValue(QString("Starting node Y"), startingNodeY);
    this->startingNodeY = startingNodeY;
  }
  else
    invalidValue(QString("starting node Y"), DEFAULT_STARTING_NODE_Y);
}

void FileParser::setTargetNodeX(const uint& targetNodeX)
{
  if (targetNodeX <= mapWidth)
  {
    validValue(QString("Target node X"), targetNodeX);
    this->targetNodeX = targetNodeX;
  }
  else
    invalidValue(QString("target node X"), DEFAULT_TARGET_NODE_X);
}

void FileParser::setTargetNodeY(const uint& targetNodeY)
{
  if (targetNodeY <= mapWidth)
  {
    validValue(QString("Target node Y"), targetNodeY);
    this->targetNodeY = targetNodeY;
  }
  else
    invalidValue(QString("target node Y"), DEFAULT_TARGET_NODE_Y);
}

void FileParser::setBuildTargetNodeSubtree(const bool& buildTargetSubtree)
{
  validValue(QString("Build target subtree"), buildTargetSubtree);
  this->buildTargetSubtree = buildTargetSubtree;
}

void FileParser::setMaxStepSize(const double& maxStepSize)
{
  if (maxStepSize > 1)
  {
    validValue(QString("Maximum step size"), maxStepSize);
    this->maxStepSize = maxStepSize;
  }
  else
    invalidValue(QString("maximum step size"), DEFAULT_MAX_STEP_SIZE);
}

void FileParser::setMaxNodesNumber(const uint& maxNodesNumber)
{
  if (maxNodesNumber > 1)
  {
    validValue(QString("Maximum number of nodes"), maxNodesNumber);
    this->maxNodesNumber = maxNodesNumber;
  }
  else
    invalidValue(QString("maximum number of nodes"), DEFAULT_MAX_NODES_NUMBER);
}

void FileParser::setRRTUpdateInterval(const uint& RRTUpdateInterval)
{
  if (RRTUpdateInterval >= 1)
  {
    validValue(QString("RRT update interval"), RRTUpdateInterval);
    this->RRTUpdateInterval = RRTUpdateInterval;
  }
  else
    invalidValue(QString("RRT update interval"), DEFAULT_RRT_UPDATE_INTERVAL);
}

void FileParser::setMaxNeighboursRadius(const double& maxNeighboursRadius)
{
  if (maxNeighboursRadius > 1)
  {
    validValue(QString("Max neighbours radius"), maxNeighboursRadius);
    this->maxNeighboursRadius = maxNeighboursRadius;
  }
  else
    invalidValue(QString("max neighbours radius"), DEFAULT_MAX_NEIGHBOURS_RADIUS);
}

void FileParser::setRRTStarUpdateInterval(const uint& RRTStarUpdateInterval)
{
  if (RRTStarUpdateInterval >= 1)
  {
    validValue(QString("RRT* update interval"), RRTStarUpdateInterval);
    this->RRTStarUpdateInterval = RRTStarUpdateInterval;
  }
  else
    invalidValue(QString("RRT* update interval"), DEFAULT_RRTSTAR_UPDATE_INTERVAL);
}

void FileParser::setTreeRedrawInterval(const uint& edgesUpdateInterval)
{
  if (edgesUpdateInterval >= 1)
  {
    validValue(QString("Edges update interval"), edgesUpdateInterval);
    this->treeRedrawInterval = edgesUpdateInterval;
  }
  else
    invalidValue(QString("edges update"), DEFAULT_TREE_REDRAW_INTERVAL);
}

void FileParser::setDijkstraUpdateInterval(const uint& DijkstraUpdateInterval)
{
  if (DijkstraUpdateInterval >= 1)
  {
    validValue(QString("Dijkstra update interval"), DijkstraUpdateInterval);
    this->DijkstraUpdateInterval = DijkstraUpdateInterval;
  }
  else
    invalidValue(QString("Dijkstra update interval"), DEFAULT_DIJKSTRA_UPDATE_INTERVAL);
}

void FileParser::setDrawQuadTreesBoundaries(const bool& drawQuadTreesBoundaries)
{
  validValue(QString("Draw quadtrees boundaries"), drawQuadTreesBoundaries);
  this->drawQuadTreesBoundaries = drawQuadTreesBoundaries;
}

void FileParser::setZones()
{
  int zoneIndex = 1;

  foreach (QString group, settings.childGroups())
  {
    settings.beginGroup(group);

    const QStringList vertices = settings.childKeys();
    QVector<uint> verticesX, verticesY;

    bool maybeIsValid = true;
    int verticeIndex = 1;
    foreach (const QString &coordinates, vertices)
    {
      if (coordinates.startsWith("X"))
      {
        bool ok;
        uint x = settings.value(coordinates).toUInt(&ok);
        if (ok)
          verticesX.push_back(x);
        else
        {
          maybeIsValid = false;
          std::cerr << "Vertice coordinate X #" << verticeIndex << " of obstacle #" << zoneIndex << " is not valid.\n => Skipping obstacle " << zoneIndex << ".\n\n";
          break;
        }
      }
      else if (coordinates.startsWith("Y"))
      {
        bool ok;
        uint y = settings.value(coordinates).toUInt(&ok);
        if (ok)
          verticesY.push_back(y);
        else
        {
          maybeIsValid = false;
          std::cerr << "Vertice coordinate Y #" << verticeIndex << "of obstacle #" << zoneIndex << " is not valid.\n => Skipping obstacle " << zoneIndex << ".\n\n";
          break;
        }
      }
      verticeIndex++;
    }
    if (maybeIsValid)
    {
      if (verticesX.size() == verticesY.size())
      {
        QPolygon zone;
        for (int i = 0; i < verticesX.size(); ++i)
          zone.push_back(QPoint(verticesX[i], verticesY[i]));

        if (group.startsWith("OBSTACLE"))
        {
          zoneIndex++;

          if (isZoneValid(zone))
            obstacles.push_back(zone);
          else
            std::cerr << "Zone '" << group.toStdString() << "' contains starting node or target node.\n => Skipping.\n";
        }
        else if (group.startsWith("OFF_LIMITS_ZONE"))
        {
          zoneIndex++;

          if (isZoneValid(zone))
            offLimitsZones.push_back(zone);
          else
            std::cerr << "Zone '" << group.toStdString() << "' contains starting node or target node.\n => Skipping.\n";
        }
        else if (group.startsWith("PREFERRED_ZONE"))
        {
          zoneIndex++;

          preferredZones.push_back(zone);
        }
      }
      else
        std::cerr << "Vertices coordinates number mismatch.\n => Skipping obstacle " << zoneIndex << ".\n\n";
    }

    settings.endGroup();
  }

  std::cerr << "Successfully read " << obstacles.size() + offLimitsZones.size() + preferredZones.size() << " zones.\n\n";
}

bool FileParser::isZoneValid(const QPolygon& zone)
{
  if (zone.containsPoint(QPoint(startingNodeX, startingNodeY), Qt::OddEvenFill) ||
      zone.containsPoint(QPoint(targetNodeX, targetNodeY), Qt::OddEvenFill))
    return false;

  return true;
}

QString FileParser::getConfigFile() const
{
  return configFile;
}

uint FileParser::getMapWidth() const
{
  return mapWidth;
}

uint FileParser::getMapHeight() const
{
  return mapHeight;
}

uint FileParser::getStartingNodeX() const
{
  return startingNodeX;
}

uint FileParser::getStartingNodeY() const
{
  return startingNodeY;
}

uint FileParser::getTargetNodeX() const
{
  return targetNodeX;
}

uint FileParser::getTargetNodeY() const
{
  return targetNodeY;
}

bool FileParser::getBuildTargetSubtree() const
{
  return buildTargetSubtree;
}

double FileParser::getMaxStepSize() const
{
  return maxStepSize;
}

uint FileParser::getMaxNodesNumber() const
{
  return maxNodesNumber;
}

uint FileParser::getRRTUpdateInterval() const
{
  return RRTUpdateInterval;
}

double FileParser::getMaxNeighbourRadius() const
{
  return maxNeighboursRadius;
}

uint FileParser::getRRTStarUpdateInterval() const
{
  return RRTStarUpdateInterval;
}

uint FileParser::getTreeRedrawInterval() const
{
  return treeRedrawInterval;
}

uint FileParser::getDijkstraUpdateInterval() const
{
  return DijkstraUpdateInterval;
}

bool FileParser::getDrawQuadTreesBoundaries() const
{
  return drawQuadTreesBoundaries;
}

QVector<QPolygon> FileParser::getObstacles() const
{
  return obstacles;
}

QVector<QPolygon> FileParser::getOffLimitsZones() const
{
  return offLimitsZones;
}

QVector<QPolygon> FileParser::getPreferredZones() const
{
  return preferredZones;
}
