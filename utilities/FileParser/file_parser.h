#ifndef FILE_PARSER_H
#define FILE_PARSER_H

#include <QtGui/QPolygon>

#include <errno.h>
#include <iostream>
#include <fstream>
#include <QSettings>

class FileParser
{
	public:
		FileParser(const QString& fileName);
		static void showVersion();

		QString getConfigFile() const;
		uint getMapWidth() const;
		uint getMapHeight() const;
		uint getStartingNodeX() const;
		uint getStartingNodeY() const;
		uint getTargetNodeY() const;
		uint getTargetNodeX() const;
		bool getBuildTargetSubtree() const;
		double getMaxStepSize() const;
		uint getMaxNodesNumber() const;
		uint getRRTUpdateInterval() const;
		double getMaxNeighbourRadius() const;
    uint getRRTStarUpdateInterval() const;
    uint getTreeRedrawInterval() const;
		uint getDijkstraUpdateInterval() const;
		bool getDrawQuadTreesBoundaries() const;
		QVector<QPolygon> getObstacles() const;
		QVector<QPolygon> getOffLimitsZones() const;
		QVector<QPolygon> getPreferredZones() const;

	private:
		QSettings settings;
		QString configFile;

		uint mapWidth, mapHeight;
		uint startingNodeX, startingNodeY;
		uint targetNodeX, targetNodeY;

		bool buildTargetSubtree;
		double maxStepSize;
		uint maxNodesNumber;
		uint RRTUpdateInterval;

		double maxNeighboursRadius;
    uint RRTStarUpdateInterval;
    uint treeRedrawInterval;

		uint DijkstraUpdateInterval;
		bool drawQuadTreesBoundaries;

		QVector<QPolygon> obstacles;
		QVector<QPolygon> offLimitsZones;
		QVector<QPolygon> preferredZones;

		void initVariables();
    void validValue(const QString& validValueName, const QVariant& value);
		void invalidValue(const QString& invalidValueName, const QVariant& defaultValue);

		void setMapWidth(const uint& mapWidth);
		void setMapHeight(const uint& mapHeight);
		void setStartingNodeX(const uint& startingNodeX);
		void setStartingNodeY(const uint& startingNodeY);
		void setTargetNodeX(const uint& targetNodeX);
		void setTargetNodeY(const uint& targetNodeY);

		void setBuildTargetNodeSubtree(const bool& buildTargetSubtree);
		void setMaxStepSize(const double& maxStepSize);
		void setMaxNodesNumber(const uint& maxNodesNumber);
		void setRRTUpdateInterval(const uint& RRTUpdateInterval);

		void setMaxNeighboursRadius(const double& maxNeighboursRadius);
		void setRRTStarUpdateInterval(const uint& RRTStarUpdateInterval);
    void setTreeRedrawInterval(const uint& treeRedrawInterval);

		void setDijkstraUpdateInterval(const uint& DijkstraUpdateInterval);
		void setDrawQuadTreesBoundaries(const bool& drawQuadTreesBoundaries);

		void setZones();
		bool isZoneValid(const QPolygon& zone);
};

#endif // FILE_PARSER_H
