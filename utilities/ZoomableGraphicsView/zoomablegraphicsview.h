#ifndef ZOOMABLEGRAPHICSVIEW_H
#define ZOOMABLEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

class ZoomableGraphicsView : public QGraphicsView
{
		Q_OBJECT

	public:
		ZoomableGraphicsView(QWidget* parent = 0);
		ZoomableGraphicsView(QGraphicsScene* scene, QWidget* parent = 0);
		void enableZoom();
		void disableZoom();

		qreal zoomFactor() const;
		void setZoomFactor(const qreal& zoomFactor);

	protected:
		// QWidget interface
		void wheelEvent(QWheelEvent* we);
		void mousePressEvent(QMouseEvent* me);
		void mouseMoveEvent(QMouseEvent* me);
		void keyPressEvent(QKeyEvent* ke);
		void keyReleaseEvent(QKeyEvent* ke);

	private:
		bool _zoomable, _zoom;
		qreal _zoomFactor;
		QPointF _currentPoint;
		QPointF _currentClickedPoint;

	signals:
		void mouse_moved(const qreal& x, const qreal& y);
		void mouse_clicked(const qreal& x, const qreal& y);
};

#endif // ZOOMABLEGRAPHICSVIEW_H
