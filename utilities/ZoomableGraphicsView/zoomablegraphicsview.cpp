#include "zoomablegraphicsview.h"

#define DEFAULT_ZOOM_FACTOR 1.05

ZoomableGraphicsView::ZoomableGraphicsView(QWidget* parent) :
	QGraphicsView(parent), _zoomable(false), _zoom(false), _zoomFactor(DEFAULT_ZOOM_FACTOR)
{
	viewport()->setMouseTracking(true);
}

ZoomableGraphicsView::ZoomableGraphicsView(QGraphicsScene* scene, QWidget* parent) :
	QGraphicsView(scene, parent), _zoomable(false), _zoom(false), _zoomFactor(DEFAULT_ZOOM_FACTOR)
{
	viewport()->setMouseTracking(true);
}

void ZoomableGraphicsView::enableZoom()
{
	_zoomable = true;
}

void ZoomableGraphicsView::disableZoom()
{
	_zoomable = false;
}

void ZoomableGraphicsView::wheelEvent(QWheelEvent* we)
{
	if (_zoomable && _zoom)
	{
		if (we->delta() > 0)
			scale(_zoomFactor, _zoomFactor);
		else
			scale(1 / _zoomFactor, 1 / _zoomFactor);

		centerOn(_currentPoint);
	}
	else
		QGraphicsView::wheelEvent(we);
}

void ZoomableGraphicsView::mousePressEvent(QMouseEvent* me)
{
	_currentClickedPoint = mapToScene(me->pos());

	emit mouse_clicked(_currentClickedPoint.x(), _currentClickedPoint.y());
	QGraphicsView::mousePressEvent(me);
}

void ZoomableGraphicsView::mouseMoveEvent(QMouseEvent* me)
{
	_currentPoint = mapToScene(me->pos());

	emit mouse_moved(_currentPoint.x(), _currentPoint.y());
	QGraphicsView::mouseMoveEvent(me);
}

void ZoomableGraphicsView::keyPressEvent(QKeyEvent* ke)
{
	if (ke->key() == Qt::Key_Control)
		_zoom = true;

	QGraphicsView::keyPressEvent(ke);
}

void ZoomableGraphicsView::keyReleaseEvent(QKeyEvent* ke)
{
	if (ke->key() == Qt::Key_Control)
		_zoom = false;

	QGraphicsView::keyReleaseEvent(ke);
}

qreal ZoomableGraphicsView::zoomFactor() const
{
	return _zoomFactor;
}

void ZoomableGraphicsView::setZoomFactor(const qreal& zoomFactor)
{
	_zoomFactor = zoomFactor;
}
