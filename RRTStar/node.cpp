#include "node.h"
#include <assert.h>
#include <QtMath>

node::node(const int& x, const int& y) :
  basenode (x, y),
	_parent(0), _cost(-1)
{}

node* node::parent() const
{
	return _parent;
}

void node::setParent(node* parent)
{
	_parent = parent;
}

qreal node::cost() const
{
	return _cost;
}

void node::setCost(const qreal& cost)
{
	_cost = cost;
}

void node::add_child(node* child)
{
	_children.push_back(child);
}

void node::remove_child(node* child)
{
	bool found = false;
	for (int i = 0; i < _children.size() && !found; ++i)
		if (_children[i] == child)
		{
			_children.removeAt(i);
			found = true;
		}
}
