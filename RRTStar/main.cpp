#include "../utilities/FileParser/file_parser.h"
#include "rrtstar.h"

#include <QApplication>
#include <QCommandLineParser>

#define DEFAULT_CONF_FILE "../settings.conf"

void showVersion()
{
	std::cout << "RRT* algorithm, version 1.0.\n"
						<< "Changelog:\n"
						<< "v1.0:\n"
						<< " - Updated to follow standard of fileParser.\n\n"
						<< "v0.1:\n"
						<< " - Initial command line options setup.\n "
						<< std::endl;
}

int main(int argc, char *argv[])
{
	QApplication application (argc, argv);

	QCommandLineParser commandLineParser;
	commandLineParser.setApplicationDescription("RRT*");

	QCommandLineOption configurationFileOption(QStringList() << "f" << "file", "The configuration file to parse.", "conf_file");
	configurationFileOption.setDefaultValue(DEFAULT_CONF_FILE);
	commandLineParser.addOption(configurationFileOption);
	const QCommandLineOption helpOption = commandLineParser.addHelpOption();
	const QCommandLineOption versionOption = commandLineParser.addVersionOption();

	if (!commandLineParser.parse(application.arguments()))
	{
		std::cerr << "Bad command line option: " << commandLineParser.errorText().toStdString() << std::endl;
		commandLineParser.showHelp();
		return 0;
	}

	if (commandLineParser.isSet(versionOption))
	{
		showVersion();
		return 0;
	}

	if (commandLineParser.isSet(helpOption))
	{
		commandLineParser.showHelp();
		return 0;
	}

	const QString fileName = commandLineParser.value(configurationFileOption);
	QFile configFile(fileName);
	if (!configFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		std::cerr << "Bad configuration file.\n => " << fileName.toStdString() << ": " << strerror(errno) << ".\n" << std::endl;
		return 0;
	}
	else
		configFile.close();

	FileParser fileParser(fileName);
	RRTStar RRTStarWindow(fileParser.getMapWidth(), fileParser.getMapHeight(),
												fileParser.getStartingNodeX(), fileParser.getStartingNodeY(),
												fileParser.getTargetNodeX(), fileParser.getTargetNodeY(),
												fileParser.getMaxNodesNumber(),
												fileParser.getMaxStepSize(), fileParser.getMaxNeighbourRadius(),
												fileParser.getObstacles(),
                        fileParser.getRRTStarUpdateInterval(),
                        fileParser.getTreeRedrawInterval());
	RRTStarWindow.show();

	return application.exec();
}
