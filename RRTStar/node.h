// node definition
#ifndef NODE
#define NODE

#include "../RRT/basenode/basenode.h"
#include <QVector>

struct node : public basenode
{
  public:
    node(const int& x = -1,
         const int& y = -1);

    node* parent() const;
    void setParent(node* parent);

    qreal cost() const;
    void setCost(const qreal& cost);

    void add_child(node* child);
    void remove_child(node* child);

    QVector<node*> _children;

  private:
    node* _parent;
    qreal _cost;
};

#endif // NODE
