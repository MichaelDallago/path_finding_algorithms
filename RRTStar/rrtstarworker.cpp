#include "rrtstarworker.h"
#include <iostream>

#include <QtMath>

RRTStarWorker::RRTStarWorker(const uint& mapWidth, const uint& mapHeight,
                             const uint& startingNodeX, const uint& startingNodeY,
                             const uint& targetNodeX, const uint& targetNodeY,
                             const uint& maxNodesNumber,
                             const qreal& maxStepSize, const qreal& maxNeighboursRadius,
                             const QVector<QPolygon>& obstacles,
                             const uint& updateInterval,
                             const uint& edgesUpdateInterval) :
  mapWidth(mapWidth), mapHeight(mapHeight),
  startingNodeX(startingNodeX), startingNodeY(startingNodeY),
  targetNodeX(targetNodeX), targetNodeY(targetNodeY),
  maxNodesNumber(maxNodesNumber),
  maxStepSize(maxStepSize), maxNeighboursRadius(maxNeighboursRadius),
  obstacles(obstacles),
  updateInterval(updateInterval),
  edgesUpdateInterval(edgesUpdateInterval)
{
  qRegisterMetaType<pathType>("pathType");

  wastedTime = 0;
  image = QImage(mapWidth, mapHeight, QImage::Format_RGB32);
  image.fill(Qt::white);
  stop = false;
}

RRTStarWorker::~RRTStarWorker()
{}

bool RRTStarWorker::getStop() const
{
  return stop;
}

void RRTStarWorker::setStop(bool stop)
{
  this->stop = stop;
}

void RRTStarWorker::addNodeToMap(node* n)
{
  map[n->x()][n->y()] = OTHERNODE;
}

bool RRTStarWorker::obstacleFree(node* src, node* dst)
{
  QLineF edge(src->toPoint(), dst->toPoint());
  QPointF p;
  for(int i = 0; i < obstaclesEdges.size(); ++i)
    if (obstaclesEdges[i].intersect(edge, &p) == QLineF::BoundedIntersection)
      return false;

  return true;
}

bool RRTStarWorker::pathExists(node* lastNode)
{
  if (QLineF(lastNode->toPoint(), targetNode->toPoint()).length() <= maxStepSize &&
      obstacleFree(lastNode, targetNode))
  {
    tree.add_node(targetNode, lastNode);
    return true;
  }

  return false;
}

void RRTStarWorker::drawPath()
{
  if (targetNode->cost() >= 0)
  {
    painter.begin(&image);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::blue, 2.0));
    node* iterator = targetNode;
    while (iterator != startingNode)
    {
      painter.drawLine(iterator->edge());
      iterator = iterator->parent();
    }
    painter.end();
  }
}

void RRTStarWorker::generatePath()
{
  if (targetNode->cost() >= 0)
  {
    node* iterator = targetNode;
    while (iterator != startingNode)
    {
      path.push_back(iterator->toPoint());
      iterator = iterator->parent();
    }
    path.push_back(iterator->toPoint());

    emit newPath(path);
  }
}

void RRTStarWorker::enqueue(node* n)
{
  painter.begin(&image);
  painter.setRenderHint(QPainter::Antialiasing, true);
  painter.drawLine(n->edge());
  painter.end();

  maybeEmitUpdatedData();
}

void RRTStarWorker::rewireTree(node* n)
{
  n->setCost(n->parent()->cost() + n->edge().length());
  for (int i = 0; i < n->_children.size(); ++i)
    rewireTree(n->_children[i]);
}

void RRTStarWorker::rebuildImage()
{
  image.fill(Qt::white);
  initImage();

  painter.begin(&image);
  foreach(node* n, tree.nodes())
    painter.drawLine(n->edge());
  painter.end();

  drawPath();

  edgesTimer.restart();
}

void RRTStarWorker::maybeEmitUpdatedData()
{
  if (localTimer.elapsed() > updateInterval)
  {
    emit newData(image, tree.size(), targetNode->cost(), chronometer.elapsed() - wastedTime);
    localTimer.restart();
  }
}

void RRTStarWorker::maybeRebuildImage()
{
  QElapsedTimer local_wasted_time;
  local_wasted_time.start();

  if (edgesTimer.elapsed() > edgesUpdateInterval)
    rebuildImage();

  wastedTime += local_wasted_time.elapsed();

  maybeEmitUpdatedData();
}

void RRTStarWorker::setMap()
{
  map.resize(mapWidth + 1);

  for (uint i = 0; i < mapWidth + 1; ++i)
    map[i].fill(FREE, mapHeight);
}

void RRTStarWorker::setObstacles()
{
  for (int i = 0; i < obstacles.size(); ++i)
  {
    QRect rect = obstacles[i].boundingRect();
    for (int j = rect.x(); j < rect.x() + rect.width(); ++j)
      for (int k = rect.y(); k < rect.y() + rect.height(); ++k)
        if (obstacles[i].containsPoint(QPoint(j, k), Qt::OddEvenFill))
          map[j][k] = OBSTACLE;
  }
}

void RRTStarWorker::setObstaclesEdges()
{
  for (int i = 0; i < obstacles.size(); ++i)
    for (int j = 0; j < obstacles[i].size(); ++j)
    {
      int size = obstacles[i].size();
      if (j < size - 1)
        obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][j + 1]));
      else
        obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][0]));
    }
}

void RRTStarWorker::initImage()
{
  painter.begin(&image);
  painter.setRenderHint(QPainter::Antialiasing, true);

  painter.setPen(QPen(Qt::green));
  painter.setBrush(QBrush(Qt::green));
  painter.drawEllipse(startingNodeX, startingNodeY, 5, 5);

  painter.setPen(QPen(Qt::red));
  painter.setBrush(QBrush(Qt::red));
  painter.drawEllipse(targetNodeX, targetNodeY, 5, 5);

  painter.setPen(QPen(Qt::black));
  painter.setBrush(QBrush(Qt::gray));
  foreach (QPolygon p, obstacles)
    painter.drawPolygon(p);

  painter.end();
}

node*RRTStarWorker::randConf()
{
  node* rand_node = new node;
  while (rand_node->x() == -1 || rand_node->y() == -1)
  {
    int x = rand() % (mapWidth);
    int y = rand() % (mapHeight);

    if (nodeStatus(x, y) == FREE)
      rand_node->set_coordinates(x, y);
  }
  return rand_node;
}

node*RRTStarWorker::steer(node* nearest, node* rand)
{
  node* new_node = new node;

  QLineF edge (nearest->toPoint(), rand->toPoint());
  if (edge.length() <= maxStepSize)
    new_node->set_coordinates(rand->x(), rand->y());
  else
  {
    edge.setLength(maxStepSize);
    QPointF p2 = edge.p2();

    if (nearest->x() < rand->x())
    {
      if (nearest->y() < rand->y())
        edge.setP2(QPoint(qFloor(p2.x()), qFloor(p2.y())));
      else
        edge.setP2(QPoint(qFloor(p2.x()), qCeil(p2.y())));
    }
    else
    {
      if (nearest->y() > rand->y())
        edge.setP2(QPoint(qCeil(p2.x()), qCeil(p2.y())));
      else
        edge.setP2(QPoint(qCeil(p2.x()), qFloor(p2.y())));
    }

    new_node->set_coordinates(edge.p2().x(), edge.p2().y());
  }

  return new_node;
}

QVector<node*> RRTStarWorker::near(node* newNode)
{
  QVector<node*> near_nodes;
  foreach(node* n, tree.nodes())
    if ((QLineF(newNode->toPoint(), n->toPoint()).length() <= maxNeighboursRadius) &&
        (obstacleFree(newNode, n)))
      near_nodes.push_back(n);

  return near_nodes;
}

int RRTStarWorker::nodeStatus(const uint& x, const uint& y)
{
  return map[x][y];
}

void RRTStarWorker::start()
{
  localTimer.start();
  edgesTimer.start();
  chronometer.start();

  setMap();
  setObstacles();
  setObstaclesEdges();
  initImage();

  startingNode = new node(startingNodeX, startingNodeY);
  tree.add_starting_node(startingNode);

  targetNode = new node(targetNodeX, targetNodeY);

  bool found = false;
  qsrand(time(NULL));
  while (tree.size() < maxNodesNumber && !stop)
  {
    node* rand_node = randConf();
    node* nearest_node = tree.nearest_node(rand_node);
    node* new_node = steer(nearest_node, rand_node);
    delete rand_node;

    if (nodeStatus(new_node->x(), new_node->y()) == FREE &&
        obstacleFree(nearest_node, new_node))
    {
      QVector<node*> X_near = near(new_node);
      node* x_min = nearest_node;
      qreal c_min = nearest_node->cost() + QLineF(nearest_node->toPoint(), new_node->toPoint()).length();
      foreach (node* x_near, X_near)
      {
        QLineF line (x_near->toPoint(), new_node->toPoint());
        if (x_near->cost() + line.length() < c_min)
        {
          x_min = x_near;
          c_min = x_near->cost() + line.length();
        }
      }
      tree.add_node(new_node, x_min);
      addNodeToMap(new_node);
      enqueue(new_node);

      foreach (node* x_near, X_near)
      {
        QLineF line (new_node->toPoint(), x_near->toPoint());
        if (new_node->cost() + line.length() < x_near->cost())
        {
          x_near->parent()->remove_child(x_near);
          x_near->setParent(new_node);
          new_node->add_child(x_near);
          x_near->setEdge(QLine(new_node->toPoint(), x_near->toPoint()));
          rewireTree(x_near);
        }
      }

      if (!found)
        if (pathExists(new_node))
          found = true;
    }
    else
      delete new_node;

    maybeRebuildImage();
  }
  rebuildImage();
  emit newData(image, tree.size(), targetNode->cost(), chronometer.elapsed());
  generatePath();
  std::cout << chronometer.elapsed() << std::endl << chronometer.elapsed() - wastedTime << std::endl;

  emit finished();
}
