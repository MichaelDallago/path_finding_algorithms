#include <assert.h>
#include <iostream>
#include <tree.h>

#include <QtMath>

Tree::Tree()
{}

Tree::~Tree()
{
  foreach (node* n, _nodes)
    delete n;
}

void Tree::add_starting_node(node* start)
{
  _start = start;
  _nodes.push_back(start);
}

void Tree::add_target_node(node* target)
{
  _target = target;
  _nodes.push_back(target);
}

void Tree::add_node(node* new_node, node* parent_node)
{
  assert(new_node);
  assert(parent_node);

  // Add node to his parent's children
  parent_node->add_child(new_node);

  // Setting node's parent
  new_node->setParent(parent_node);

  // Setting node's edge
  new_node->setEdge(QLine(parent_node->toPoint(), new_node->toPoint()));

  // Setting cost
  new_node->setCost(parent_node->cost() + QLineF(new_node->edge()).length());

  // Adding node to _nodes
  _nodes.push_back(new_node);
}

node* Tree::nearest_node(node* rand_node)
{
  node* nearest = 0;
  qreal tmp = 0., dst = INT64_MAX;
  foreach(node* s, _nodes)
    if ((tmp = distance(s, rand_node)) < dst)
    {
      dst = tmp;
      nearest = s;
    }

  return nearest;
}

size_t Tree::size() const
{
  return _nodes.size();
}

QVector<node*> Tree::nodes() const
{
  return _nodes;
}

qreal Tree::distance(node* src, node* dst)
{
  return qSqrt(qPow(dst->x() - src->x(), 2) + qPow(dst->y() - src->y(), 2));
}
