#ifndef RRTSTARWORKER_H
#define RRTSTARWORKER_H

#include <tree.h>
#include <node.h>

#include <QElapsedTimer>
#include <QImage>
#include <QObject>
#include <QPainter>

#define FREE			0
#define OBSTACLE	1
#define OTHERNODE	2

typedef QVector<QPoint> pathType;

class RRTStarWorker : public QObject
{
		Q_OBJECT
	public:
		RRTStarWorker(const uint& mapWidth, const uint& mapHeight,
									const uint& startingNodeX, const uint& startingNodeY,
									const uint& targetNodeX, const uint& targetNodeY,
									const uint& maxNodesNumber,
									const qreal& maxStepSize, const qreal& maxNeighboursRadius,
									const QVector<QPolygon>& obstacles,
                  const uint& updateInterval,
                  const uint& edgesUpdateInterval);
		~RRTStarWorker();

		bool getStop() const;
		void setStop(bool stop);

	private:
		uint mapWidth, mapHeight;
		uint startingNodeX, startingNodeY;
		uint targetNodeX, targetNodeY;
		uint maxNodesNumber;
		qreal maxStepSize, maxNeighboursRadius;
		QVector<QPolygon> obstacles;
    uint updateInterval;
    uint edgesUpdateInterval;

		QVector<QLineF> obstaclesEdges;
    QElapsedTimer chronometer, edgesTimer, localTimer;
		pathType path;
		bool stop;
		qint64 wastedTime;

    QVector<QVector<int>> map;
		node *startingNode, *targetNode;
    Tree tree;
		QImage image;
		QPainter painter;

		void setMap();
		void setObstacles();
		void setObstaclesEdges();
		void initImage();

		node* randConf();
		node* steer(node* nearest, node* rand);
		QVector<node*> near(node* newNode);

		int nodeStatus(const uint& x, const uint& y);
		void addNodeToMap(node* n);
		bool obstacleFree(node* src, node* dst);
		bool pathExists(node* lastNode);
		void drawPath();
    void generatePath();
    void enqueue(node* n);
		void rewireTree(node* n);
    void rebuildImage();
    void maybeRebuildImage();
    void maybeEmitUpdatedData();

	public slots:
		void start();

	signals:
    void newData(QImage, int, qreal, qint64);
    void finished();
		void newPath(pathType);
};

#endif // RRTSTARWORKER_H
