#include "rrtstar.h"
#include "ui_rrtstar.h"

#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QTimer>
#include <QTextStream>
#include <QBitmap>

#define DEFAULT_PATH_FILE "../RRTStarpath"

RRTStar::RRTStar(const uint& mapWidth, const uint& mapHeight,
                 const uint& startingNodeX, const uint& startingNodeY,
                 const uint& targetNodeX, const uint& targetNodeY,
                 const uint& maxNodesNumber,
                 const qreal& maxStepSize, const qreal& maxNeighboursRadius,
                 const QVector<QPolygon>& obstacles,
                 const uint& updateInterval,
                 const uint& edgesUpdateInterval) :
  ui(new Ui::RRTStar),
  thread(nullptr), workerThread(nullptr),
  mapWidth(mapWidth), mapHeight(mapHeight),
  startingNodeX(startingNodeX), startingNodeY(startingNodeY),
  targetNodeX(targetNodeX), targetNodeY(targetNodeY),
  maxNodesNumber(maxNodesNumber),
  maxStepSize(maxStepSize), maxNeighboursRadius(maxNeighboursRadius),
  obstacles(obstacles),
  updateInterval(updateInterval),
  edgesUpdateInterval(edgesUpdateInterval)
{
  ui->setupUi(this);

  graphicsScene = new QGraphicsScene(this);
  graphicsScene->setSceneRect(0, 0, mapWidth, mapHeight);

  ui->graphicsView->setScene(graphicsScene);
  ui->graphicsView->enableZoom();
  ui->menuBar->setEnabled(false);

  QTimer::singleShot(10, this, &RRTStar::begin);
}

void RRTStar::begin()
{
  thread.reset(new QThread);
  workerThread.reset(new RRTStarWorker(mapWidth, mapHeight,
                                       startingNodeX, startingNodeY,
                                       targetNodeX, targetNodeY,
                                       maxNodesNumber,
                                       maxStepSize, maxNeighboursRadius,
                                       obstacles,
                                       updateInterval,
                                       edgesUpdateInterval));
  workerThread->moveToThread(thread.data());

  connect(thread.data(), &QThread::started, workerThread.data(), &RRTStarWorker::start);
  connect(workerThread.data(), &RRTStarWorker::newData, this, &RRTStar::catch_data);
  connect(workerThread.data(), &RRTStarWorker::newPath, this, &RRTStar::catch_path);
  connect(workerThread.data(), &RRTStarWorker::finished, thread.data(), &QThread::quit, Qt::DirectConnection);
  connect(workerThread.data(), &RRTStarWorker::finished, [&]()
  {
    ui->stopButton->setEnabled(false);
  });

  thread->start();
}

void RRTStar::savePathToFile(const QString& fileName)
{
  QFile file(fileName);

  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    std::cerr << "Failed to create file " << file.fileName().toStdString() << ": " << strerror(errno);
    return;
  }

  QTextStream out(&file);
  for (int i = pathNodes.size() - 1; i >= 0; --i)
    out << pathNodes[i].x() << " " << pathNodes[i].y() << "\n";

  file.close();

  ui->statusBar->showMessage("File " + fileName + " successfully saved.");
}

RRTStar::~RRTStar()
{
  workerThread->setStop(true);
  thread->wait();

  delete ui;
}

void RRTStar::closeEvent(QCloseEvent* closeEvent)
{
  QMessageBox::StandardButton resBtn = QMessageBox::question(this,
                                                             tr("RRT"),
                                                             tr("Are you sure?\n"),
                                                             QMessageBox::No | QMessageBox::Yes,
                                                             QMessageBox::Yes);
  if (resBtn != QMessageBox::Yes)
    closeEvent->ignore();
  else
    closeEvent->accept();
}

void RRTStar::catch_data(QImage image, int nodes, qreal cost, qint64 elapsedTime)
{
  graphicsScene->clear();
  graphicsScene->addPixmap(QPixmap::fromImage(image));

  ui->nodesNumberLCDNumber->display((int) nodes);
  ui->elapsedTimeLCDNumber->display((int) elapsedTime);
  ui->pathCostLCDNumber->display((double) cost);
}

void RRTStar::catch_path(QVector<QPoint> path)
{
  pathNodes = path;
  savePathToFile(DEFAULT_PATH_FILE);
  ui->menuBar->setEnabled(true);
}

void RRTStar::on_stopButton_clicked()
{
  ui->stopButton->setEnabled(false);
  workerThread->setStop(true);
}

void RRTStar::on_actionSave_path_as_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  tr("Save file"),
                                                  "../");
  savePathToFile(fileName);
}
