#-------------------------------------------------
#
# Project created by QtCreator 2015-11-24T11:37:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RRTStar
TEMPLATE = app

SOURCES += main.cpp \
    rrtstar.cpp \
    rrtstarworker.cpp \
    ../RRT/basenode/basenode.cpp \
    tree.cpp \
    ../utilities/FileParser/file_parser.cpp \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.cpp \
    node.cpp

HEADERS  += \
    rrtstar.h \
    rrtstarworker.h \
    ../RRT/basenode/basenode.h \
    tree.h \
    ../utilities/FileParser/file_parser.h \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.h \
    node.h

FORMS    += rrtstar.ui

CONFIG += c++11

DISTFILES +=

