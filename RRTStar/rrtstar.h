#ifndef RRTSTAR_H
#define RRTSTAR_H

#include "rrtstarworker.h"
#include <QCloseEvent>
#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QThread>

namespace Ui {
	class RRTStar;
}

class RRTStar : public QMainWindow
{
		Q_OBJECT

	public:
		RRTStar(const uint& mapWidth, const uint& mapHeight,
						const uint& startingNodeX, const uint& startingNodeY,
						const uint& targetNodeX, const uint& targetNodeY,
						const uint& maxNodesNumber, const qreal& maxStepSize,
						const qreal& maxNeighboursRadius,
						const QVector<QPolygon>& obstacles,
            const uint& updateInterval,
            const uint& edgesUpdateInterval);
		~RRTStar();

	protected:
		void closeEvent(QCloseEvent* closeEvent);

	private:
		Ui::RRTStar *ui;
		QGraphicsScene *graphicsScene;
    QScopedPointer<QThread> thread;
    QScopedPointer<RRTStarWorker> workerThread;
    QVector<QPoint> pathNodes;

		int mapWidth, mapHeight;
		int startingNodeX, startingNodeY;
		int targetNodeX, targetNodeY;
		unsigned int maxNodesNumber;
		qreal maxStepSize, maxNeighboursRadius;
		QVector<QPolygon> obstacles;
    int updateInterval;
    uint edgesUpdateInterval;

		void begin();
		void savePathToFile(const QString& fileName);

	private slots:
    void catch_data(QImage image, int nodes, qreal cost, qint64 elapsedTime);
		void catch_path(pathType);

		void on_stopButton_clicked();
		void on_actionSave_path_as_triggered();
};

#endif // RRTSTAR_H
