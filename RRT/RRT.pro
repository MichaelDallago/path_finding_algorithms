#-------------------------------------------------
#
# Project created by QtCreator 2015-11-05T11:25:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RRT
TEMPLATE = app


SOURCES += main.cpp\
        rrt.cpp \
    rrtworker.cpp \
    tree.cpp \
    basenode/basenode.cpp \
    ../utilities/FileParser/file_parser.cpp \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.cpp \
    node.cpp

HEADERS  += rrt.h \
    rrtworker.h \
    tree.h \
    basenode/basenode.h \
    ../utilities/FileParser/file_parser.h \
    ../utilities/ZoomableGraphicsView/zoomablegraphicsview.h \
    node.h

FORMS    += rrt.ui

CONFIG  += c++11
