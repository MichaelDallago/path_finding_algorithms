#include "node.h"

node::node(const int& x, const int& y) :
  basenode(x, y),
	_parent(0),
	_membership(UNKNOWN)
{}

type node::membership() const
{
	return _membership;
}

void node::setMembership(const type& membership)
{
	_membership = membership;
}

node* node::parent() const
{
	return _parent;
}

void node::setParent(node* parent)
{
	_parent = parent;
}

void node::add_child(node* node)
{
	_children.push_back(node);
}

QVector<node*> node::children() const
{
	return _children;
}
