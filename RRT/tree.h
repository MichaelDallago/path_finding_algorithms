#ifndef TREE_H
#define TREE_H

#include <node.h>

class Tree
{
	private:
    node* _start;
    node* _target;
		uint _size;
    QVector<node*> _nodes;

    qreal _distance(node* src, node* dst);

	public:
		Tree();
    ~Tree();
    void add_starting_node(node* start);
    void add_target_node(node* target);
    void add_node(node* new_node, node* nearest);
    node* nearest_node(node* rand_node);
    int size() const;
    QVector<node*> nodes() const;
};

#endif // TREE_H
