#include "basenode.h"

basenode::basenode(const int& x, const int& y) :
  _x(x), _y(y)
{}

int basenode::x() const
{
	return _x;
}

void basenode::setX(int x)
{
	_x = x;
}

int basenode::y() const
{
	return _y;
}

void basenode::setY(int y)
{
	_y = y;
}

void basenode::set_coordinates(const int& x, const int& y)
{
	_x = x;
	_y = y;
}

QLineF basenode::edge() const
{
	return _edge;
}

void basenode::setEdge(const QLineF& edge)
{
	_edge = edge;
}

QPoint basenode::toPoint() const
{
	return QPoint(_x, _y);
}

QPointF basenode::toPointF() const
{
	return QPointF(_x, _y);
}
