#ifndef BASENODE
#define BASENODE

#include <QLineF>

struct basenode
{
	public:
		basenode(const int& x = -1,
             const int& y = -1);

		int x() const;
		void setX(int x);

		int y() const;
		void setY(int y);

		void set_coordinates(const int& x, const int& y);

    QLineF edge() const;
		void setEdge(const QLineF& edge);

		QPoint toPoint() const;
		QPointF toPointF() const;

	private:
		int _x, _y;
    QLineF _edge;
};

#endif // CNODE

