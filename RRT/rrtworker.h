#ifndef RRTWORKER_H
#define RRTWORKER_H

#include <tree.h>
#include <node.h>

#include <QElapsedTimer>
#include <QImage>
#include <QObject>
#include <QPainter>

#define FREE			0
#define OBSTACLE	1
#define NODE			2

typedef QVector<QPoint> pathType;

class RRTWorker : public QObject
{
		Q_OBJECT
	public:
		RRTWorker(const uint& mapWidth, const uint& mapHeight,
							const uint& startingNodeX, const uint& startingNodeY,
							const uint& targetNodeX, const uint& targetNodeY,
							const bool& buildTargetSubtree,
							const uint& maxNodesNumber, const qreal& maxStepSize,
							const uint& updateInterval,
							const QVector<QPolygon>& obstacles);
		~RRTWorker();

		bool getStop() const;
		void setStop(bool stop);

	private:
		uint mapWidth, mapHeight;
		uint startingNodeX, startingNodeY;
		uint targetNodeX, targetNodeY;
		bool buildTargetSubtree;
		uint maxNodesNumber;
		qreal maxStepSize;
		uint updateInterval;
		QVector<QPolygon> obstacles;
		QVector<QLineF> obstaclesEdges;
		QElapsedTimer timer;
		bool stop;

    node *startingNode, *targetNode;
		QVector<QVector<int>> map;
    Tree T;
		QImage image;
		QPainter painter;

		void setMap();
		void setObstacles();
		void setObstaclesEdges();
		void initImage();

    node* randConf();
    node* steer(node* nearest, node* rand);
		int nodeStatus(const uint& x, const uint& y);
    void addNodeToMap(node* n);
    bool obstacleFree(node* src, node* dst);
    bool pathExists(node* lastNode);
    void generatePath(node* src, node* dst, QVector<QPoint>& path);
    void drawSubtreesConnection(node* subtree1, node* subtree2);
    void swapPath(QVector<QPoint>& path);
    void enqueue(node* n);
    void maybeEmitUpdatedImage();

	public slots:
		void start();

	signals:
    void newData(QImage, int);
    void finished();
    void newPath(pathType);
};

#endif // RRTWORKER_H
