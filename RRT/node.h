#ifndef NODE_H
#define NODE_H

#include "basenode/basenode.h"

#include <QVector>

enum type {START, TARGET, UNKNOWN};

class node : public basenode
{
	public:
    node(const int& x = -1,
          const int& y = -1);

		type membership() const;
		void setMembership(const type& membership);

    node* parent() const;
    void setParent(node* parent);

    void add_child(node* node);
    QVector<node*> children() const;

    QVector<node*> _children;

	private:
    node* _parent;
		type _membership;
};

#endif // NODE_H
