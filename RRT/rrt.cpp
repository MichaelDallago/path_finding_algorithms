﻿#include "rrt.h"
#include "ui_rrt.h"

#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QTimer>
#include <QTextStream>

#define DEFAULT_PATH_FILE "../RRTpath"

RRT::RRT(const uint& mapWidth, const uint& mapHeight,
         const uint& startingNodeX, const uint& startingNodeY,
         const uint& targetNodeX, const uint& targetNodeY,
         const bool& buildTargetSubtree,
         const uint& maxNodesNumber, const qreal& maxStepSize,
         const uint& updateInterval,
         const QVector<QPolygon>& obstacles) :
  ui(new Ui::RRT),
  worker(nullptr), thread(nullptr),
  mapWidth(mapWidth), mapHeight(mapHeight),
  startingNodeX(startingNodeX), startingNodeY(startingNodeY),
  targetNodeX(targetNodeX), targetNodeY(targetNodeY),
  buildTargetSubtree(buildTargetSubtree),
  maxNodesNumber(maxNodesNumber),
  maxStepSize(maxStepSize),
  updateInterval(updateInterval),
  obstacles(obstacles)
{
  ui->setupUi(this);

  graphicsScene = new QGraphicsScene(this);
  graphicsScene->setSceneRect(0, 0, mapWidth + 5, mapHeight + 5);

  ui->graphicsView->setScene(graphicsScene);
  ui->graphicsView->enableZoom();
  ui->menuBar->setEnabled(false);

  QTimer::singleShot(10, this, &RRT::begin);
}

void RRT::begin()
{
  thread.reset(new QThread);
  worker.reset(new RRTWorker(mapWidth, mapHeight,
                             startingNodeX, startingNodeY,
                             targetNodeX, targetNodeY,
                             buildTargetSubtree,
                             maxNodesNumber, maxStepSize,
                             updateInterval,
                             obstacles));
  worker->moveToThread(thread.data());

  connect(thread.data(), &QThread::started, [&]()
  {
    chronometer.start();
  });
  connect(thread.data(), &QThread::started, worker.data(), &RRTWorker::start);
  connect(worker.data(), &RRTWorker::newData, this, &RRT::catchData);
  connect(worker.data(), &RRTWorker::newPath, this, &RRT::catchPath);
  connect(worker.data(), &RRTWorker::finished, thread.data(), &QThread::quit, Qt::DirectConnection);
  connect(worker.data(), &RRTWorker::finished, [&]()
  {
    ui->stopButton->setEnabled(false);
  });

  thread.data()->start();
}

void RRT::savePathToFile(const QString& fileName)
{
  QFile file(fileName);

  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    std::cerr << "Failed to create file " << file.fileName().toStdString() << ": " << strerror(errno) << ".\n" << std::endl;
    return;
  }

  QTextStream out(&file);
  for (int i = pathNodes.size() - 1; i >= 0; --i)
    out << pathNodes[i].x() << " " << pathNodes[i].y() << "\n";

  file.close();

  ui->statusBar->showMessage("File " + fileName + " successfully saved.", 2000);
}

RRT::~RRT()
{
  worker->setStop(true);
  thread->wait();

  delete ui;
}

void RRT::catchData(QImage image, uint nodes)
{
  graphicsScene->clear();
  graphicsScene->addPixmap(QPixmap::fromImage(image));

  ui->elapsedTimeLCDNumber->display((int) chronometer.elapsed());
  ui->nodesNumberLCDNumber->display((int) nodes);
}

void RRT::catchPath(QVector<QPoint> path)
{
  pathNodes = path;
  savePathToFile(DEFAULT_PATH_FILE);
  ui->menuBar->setEnabled(true);
}

void RRT::on_stopButton_clicked()
{
  ui->stopButton->setEnabled(false);

  worker->setStop(true);
}

void RRT::on_actionSave_path_as_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  tr("Save file"),
                                                  "../");
  savePathToFile(fileName);
}
