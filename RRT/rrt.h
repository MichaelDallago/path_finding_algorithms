#ifndef RRT_H
#define RRT_H

#include <rrtworker.h>

#include <QCloseEvent>
#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QThread>

namespace Ui {
	class RRT;
}

class RRT : public QMainWindow
{
		Q_OBJECT

	public:
		RRT(const uint& mapWidth, const uint& mapHeight,
				const uint& startingNodeX, const uint& startingNodeY,
				const uint& targetNodeX, const uint& targetNodeY,
				const bool& buildTargetSubtree,
				const uint& maxNodesNumber, const qreal& maxStepSize,
				const uint& updateInterval,
				const QVector<QPolygon>& obstacles);
		~RRT();

	private:
		Ui::RRT *ui;
		QGraphicsScene *graphicsScene;
    QScopedPointer<RRTWorker> worker;
    QScopedPointer<QThread> thread;
		QElapsedTimer chronometer;
    QVector<QPoint> pathNodes;

		uint mapWidth, mapHeight;
		uint startingNodeX, startingNodeY;
		uint targetNodeX, targetNodeY;
		bool buildTargetSubtree;
		uint maxNodesNumber;
		qreal maxStepSize;
		uint updateInterval;
		QVector<QPolygon> obstacles;

		void begin();
		void savePathToFile(const QString& fileName);

	private slots:
		void catchData(QImage image, uint nodes);
    void catchPath(QVector<QPoint> path);

		void on_stopButton_clicked();
		void on_actionSave_path_as_triggered();
};

#endif // RRT_H
