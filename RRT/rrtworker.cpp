#include <QMessageBox>
#include <QtMath>

#include <iostream>
#include <rrtworker.h>
#include <tree.h>

RRTWorker::RRTWorker(const uint& mapWidth, const uint& mapHeight,
										 const uint& startingNodeX, const uint& startingNodeY,
										 const uint& targetNodeX, const uint& targetNodeY,
										 const bool& buildTargetSubtree,
										 const uint& maxNodesNumber, const qreal& maxStepSize,
										 const uint& updateInterval,
										 const QVector<QPolygon>& obstacles) :
	mapWidth(mapWidth), mapHeight(mapHeight),
	startingNodeX(startingNodeX), startingNodeY(startingNodeY),
	targetNodeX(targetNodeX), targetNodeY(targetNodeY),
	buildTargetSubtree(buildTargetSubtree),
	maxNodesNumber(maxNodesNumber), maxStepSize(maxStepSize),
	updateInterval(updateInterval),
	obstacles(obstacles)
{
	qRegisterMetaType<pathType>("pathType");

	image = QImage(mapWidth, mapHeight, QImage::Format_ARGB32);
	image.fill(Qt::white);
	stop = false;
}

RRTWorker::~RRTWorker()
{}

bool RRTWorker::getStop() const
{
	return stop;
}

void RRTWorker::setStop(bool stop)
{
	this->stop = stop;
}

void RRTWorker::addNodeToMap(node* n)
{
	map[n->x()][n->y()] = NODE;
}

bool RRTWorker::obstacleFree(node* src, node* dst)
{
  QLineF edge(src->toPoint(), dst->toPoint());
	QPointF p;
	bool not_intersecting = true;
	for(int i = 0; i < obstaclesEdges.size() && not_intersecting; ++i)
		if (obstaclesEdges[i].intersect(edge, &p) == QLineF::BoundedIntersection)
			not_intersecting = false;

	return not_intersecting;
}

bool RRTWorker::pathExists(node* lastNode)
{
  QVector<QPoint> path;
	if (!buildTargetSubtree)
	{
		if (QLineF(lastNode->toPoint(), targetNode->toPoint()).length() <= maxStepSize &&
				obstacleFree(lastNode, targetNode))
		{
      T.add_node(targetNode, lastNode);
      generatePath(targetNode, startingNode, path);
      emit newPath(path);
			return true;
		}
	}
	else
	{
    foreach(node* n, T.nodes())
    {
      if (n->membership() != lastNode->membership() &&
          QLineF(lastNode->toPointF(), n->toPointF()).length() <= maxStepSize &&
          obstacleFree(lastNode, n))
      {
        if (lastNode->membership() == START)
          generatePath(n, targetNode, path);
        else
          generatePath(lastNode, targetNode, path);

        drawSubtreesConnection(lastNode, n);
        swapPath(path);

        if (lastNode->membership() == START)
          generatePath(lastNode, startingNode, path);
        else
          generatePath(n, startingNode, path);

        emit newPath(path);
        return true;
      }
    }
	}

	return false;
}

void RRTWorker::generatePath(node* src, node* dst, QVector<QPoint>& path)
{
	painter.begin(&image);

  painter.setPen(QPen(Qt::blue, 2.0));
  node* iterator = src;
  while (iterator != dst)
	{
    path.push_back(iterator->toPoint());
    painter.drawLine(iterator->edge());
		iterator = iterator->parent();
	}
  path.push_back(iterator->toPoint());
  painter.end();
}

void RRTWorker::drawSubtreesConnection(node* subtree1, node* subtree2)
{
  painter.begin(&image);
  painter.setPen(QPen(Qt::blue, 2.0));
  painter.drawLine(QLine(subtree1->toPoint(), subtree2->toPoint()));
  painter.end();
}

void RRTWorker::swapPath(QVector<QPoint>& path)
{
  for (int i = 0; i < path.size() / 2; ++i)
  {
    QPoint tmp = path[i];
    path[i] = path[path.size() - 1 - i];
    path[path.size() - 1 - i] = tmp;
  }
}

void RRTWorker::enqueue(node* n)
{
  painter.begin(&image);
  painter.drawLine(n->edge());
	painter.end();
}

void RRTWorker::maybeEmitUpdatedImage()
{
  if (timer.elapsed() > updateInterval)
	{
    emit newData(image, T.size());
		timer.restart();
	}
}

void RRTWorker::setMap()
{
	map.resize(mapWidth + 1);
	for (int i = 0; i < map.size(); ++i)
	{
		map[i].resize(mapHeight + 1);
		map[i].fill(FREE, mapHeight + 1);
	}
}

void RRTWorker::setObstacles()
{
	for (int i = 0; i < obstacles.size(); ++i)
	{
		QRect rect = obstacles[i].boundingRect();
		for (int j = rect.x(); j <= rect.x() + rect.width(); ++j)
			for (int k = rect.y(); k <= rect.y() + rect.height(); ++k)
				if (obstacles[i].containsPoint(QPoint(j, k), Qt::OddEvenFill))
					map[j][k] = OBSTACLE;
	}
}

void RRTWorker::setObstaclesEdges()
{
	for (int i = 0; i < obstacles.size(); ++i)
		for (int j = 0; j < obstacles[i].size(); ++j)
		{
			int size = obstacles[i].size();
			if (j < size - 1)
				obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][j + 1]));
			else
				obstaclesEdges.push_back(QLineF(obstacles[i][j], obstacles[i][0]));
		}
}

void RRTWorker::initImage()
{
	painter.begin(&image);

	painter.setPen(QPen(Qt::green));
	painter.setBrush(QBrush(Qt::green));
	painter.drawEllipse(startingNodeX, startingNodeY, 5, 5);

	painter.setPen(QPen(Qt::red));
	painter.setBrush(QBrush(Qt::red));
	painter.drawEllipse(targetNodeX, targetNodeY, 5, 5);

	painter.setPen(QPen(Qt::black));
	painter.setBrush(QBrush(Qt::gray));
	foreach (QPolygon p, obstacles)
		painter.drawPolygon(p);

	painter.end();
}

node* RRTWorker::randConf()
{
  node* rand_node = new node;
  while (rand_node->x() == -1 || rand_node->y() == -1)
	{
		int x = rand() % (mapWidth);
		int y = rand() % (mapHeight);

		if (nodeStatus(x, y) == FREE)
			rand_node->set_coordinates(x, y);
	}
	return rand_node;
}

node*RRTWorker::steer(node* nearest, node* rand)
{
  node* new_node = new node;

  QLineF edge (nearest->toPoint(), rand->toPoint());
	if (edge.length() <= maxStepSize)
		new_node->set_coordinates(rand->x(), rand->y());
	else
	{
		edge.setLength(maxStepSize);
		QPointF p2 = edge.p2();

    if (nearest->x() < rand->x())
		{
      if (nearest->y() < rand->y())
				edge.setP2(QPoint(qFloor(p2.x()), qFloor(p2.y())));
			else
				edge.setP2(QPoint(qFloor(p2.x()), qCeil(p2.y())));
		}
		else
    {
      if (nearest->y() > rand->y())
				edge.setP2(QPoint(qCeil(p2.x()), qCeil(p2.y())));
			else
				edge.setP2(QPoint(qCeil(p2.x()), qFloor(p2.y())));
		}

		new_node->set_coordinates(edge.p2().x(), edge.p2().y());
	}

	return new_node;
}

int RRTWorker::nodeStatus(const uint& x, const uint& y)
{
	return map[x][y];
}

void RRTWorker::start()
{
	setMap();
	setObstacles();
	setObstaclesEdges();
	initImage();

	timer.start();
  startingNode = new node(startingNodeX, startingNodeY);
  T.add_starting_node(startingNode);

  targetNode = new node(targetNodeX, targetNodeY);
	if (buildTargetSubtree)
    T.add_target_node(targetNode);

	bool found = false;
	qsrand(time(NULL));
  while (T.size() < maxNodesNumber && !found && !stop)
	{
		maybeEmitUpdatedImage();

    node* rand_node = randConf();
    node* nearest_node = T.nearest_node(rand_node);
    node* new_node = steer(nearest_node, rand_node);
		delete rand_node;

		if (nodeStatus(new_node->x(), new_node->y()) == FREE &&
				obstacleFree(nearest_node, new_node))
		{
      T.add_node(new_node, nearest_node);

			if (pathExists(new_node))
				found = true;

			enqueue(new_node);
		}
		else
			delete new_node;
	}
  emit newData(image, T.size());

  emit finished();
}
